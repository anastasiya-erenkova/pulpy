Инструкция по развертыванию:

0. Prerequisites: docker, docker-compose

1. Делаем папку strapi. Копируем в нее docker-compose.yml. Настраиваем в docker-compose порты на сервисы, а также имена контейнеров.
2. Переходим в папку и выполняем docker-compose up. Дожидаемся старта обоих сервисов. Страпи будет стартовать достаточно долго, нужно дождаться надписи:
   NB! docker-compose использует имя папки в качестве имени проекта, таким образом, если вы захотите использовать несколько проектов, например, так:

projectA
+-- docker
| \-- docker-compose.yml
projectB
\-- docker
\-- docker-compose.yml

Ваши файлы начнут аффектить друг друга. Для решения этой проблемы нужно использовать опцию -p: https://docs.docker.com/compose/reference/overview/#use--p-to-specify-a-project-name

pulpy_strapi_dev | Project information
pulpy_strapi_dev |
pulpy_strapi_dev | ┌────────────────────┬──────────────────────────────────────────────────┐
pulpy_strapi_dev | │ Time │ Tue May 12 2020 20:49:06 GMT+0000 (Coordinated … │
pulpy_strapi_dev | │ Launched in │ 3693 ms │
pulpy_strapi_dev | │ Environment │ development │
pulpy_strapi_dev | │ Process PID │ 97 │
pulpy_strapi_dev | │ Version │ 3.0.0-beta.19.4 (node v12.13.0) │
pulpy_strapi_dev | └────────────────────┴──────────────────────────────────────────────────┘
pulpy_strapi_dev |
pulpy_strapi_dev | Actions available
pulpy_strapi_dev |
pulpy_strapi_dev | One more thing...
pulpy_strapi_dev | Create your first administrator 💻 by going to the administration panel at:
pulpy_strapi_dev |
pulpy_strapi_dev | ┌─────────────────────────────┐
pulpy_strapi_dev | │ http://localhost:1337/admin │
pulpy_strapi_dev | └─────────────────────────────┘
pulpy_strapi_dev |
pulpy_strapi_dev | [2020-05-12T20:49:06.461Z] debug HEAD /admin (14 ms) 200
pulpy_strapi_dev | [2020-05-12T20:49:06.465Z] info File created: /srv/app/extensions/users-permissions/config/jwt.json
pulpy_strapi_dev | [2020-05-12T20:49:06.467Z] info ⏳ Opening the admin panel...

3. Делаем docker-compose down.
   NB! очень важно выполнить этот шаг, если вы будете "наживую" заменять базу - сломаете postgres.
4. Копируем конфигурацию страпи: Копируем все папки в папку strapi_app с заменой содержимого.
   NB. packages.json копировать НЕ нужно.
5. Копируем содержимое базы данных.
6. Открываем админ-панель страпи по адресу <server_address>:<docker-compose_port>/admin
   NB! В БД хранятся пользователи. Администраторы хранятся в таблице strapi_administrator.
7. Делаем docker-compose up. Это перезапустить наши сервисы.
8. Закройте страпи-порт каким-нибудь reverse proxy, мы использовали nginx, чтобы получать данные из страпи по адресу <server_address>/api
