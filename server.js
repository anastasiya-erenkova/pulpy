const express = require('express');
const path = require('path');
const app = express();

var adapterFor = (function() {
  var url = require('url'),
    adapters = {
      'http:': require('http'),
      'https:': require('https'),
    };

  return function(inputUrl) {
    return adapters[url.parse(inputUrl).protocol]
  }
}());

app.use(express.static(path.join(__dirname, 'build')));

/*app.get('*', (req, res) => res.sendFile(path.join(__dirname, './build/index.html')));
*/
app.get('*', function (req, response) {
  process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

  api_url = process.env.STRAPI_URI;
  let url = `${api_url}/meta-tags`;
  let http = adapterFor(url);
  let meta_tags = {};
  getter = http.get(url,(res) => {
      let body = "";

      res.on("data", (chunk) => {
          body += chunk;
      });

      res.on("end", () => {
          try {
              let json = JSON.parse(body);
              // do something with JSON
              meta_tags = json;
              response_modified_index(response, meta_tags);
          } catch (error) {
              console.error(error.message);
          };
      });

  }).on("error", (error) => {
      console.error(error.message);
  });
});


function response_modified_index(response, meta_tags) {
  const filePath = path.resolve(__dirname, './build', 'index.html');
  fs = require('fs');
  fs.readFile(filePath, function (err, html) {
    if (err) {
        throw err; 
    }
    meta_html = meta_tags.map(element => {
      return `<meta property="${element.property}" content="${element.content}" />`;
    });
    const JSDOM = require('jsdom').JSDOM;
    jsdom = new JSDOM(html);
    const headElement = jsdom.window.document.getElementsByTagName("head")[0];
    meta_html.forEach(element => {
      headElement.insertAdjacentHTML("beforeend", element);  
    });
    modified_html = jsdom.serialize();
    response.writeHeader(200, {"Content-Type": "text/html"});  
    response.write(modified_html);  
    response.end();
  });
}

app.get('/ping', function (req, response) {
  console.log('ping executed');
  return response.send('200 OK');
});

app.listen(process.env.PORT || 80);
