const express = require('express');
const path = require('path');
const app = express();

var adapterFor = (function() {
  var url = require('url'),
    adapters = {
      'http:': require('http'),
      'https:': require('https'),
    };

  return function(inputUrl) {
    return adapters[url.parse(inputUrl).protocol]
  }
}());

let static = path.join(__dirname, 'static')

app.get('/', function(req, res) {
    res.sendFile(path.join(static, 'index.html'));
});

app.use(express.static(static));

app.get('/ping', function (req, response) {
  console.log('ping executed');
  return response.send('200 OK');
});

app.listen(process.env.PORT || 80);
