FROM node:13 as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm update -g npm
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM node:13 as production-stage
COPY --from=build-stage /app /usr/src/app
WORKDIR /usr/src/app
RUN ls
EXPOSE 80
CMD [ "node", "server.js" ]
