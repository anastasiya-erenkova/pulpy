import styled from 'styled-components';

const Description = styled.p`
  font-family: 'GothamPro', sans-serif;
  font-size: 30px;
  line-height: 1.21;
  letter-spacing: 0.01em;

  ${({ theme }) => `
    ${theme.media.desktop} {
      font-size: 24px;
    }

    ${theme.media.mobile} {
      font-size: 18px;
    }
  `}
`;

export default Description;
