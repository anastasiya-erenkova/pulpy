import React from 'react';

import { CustomButton, Back, Text } from './Button.styles';

const Button = ({ children, ...props }) => (
  <CustomButton {...props}>
    <Back importance={props.importance} />
    <Text>{children}</Text>
  </CustomButton>
);

export default Button;
