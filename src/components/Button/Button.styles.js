import styled from 'styled-components';

import CustomBack from './Back';

export const CustomButton = styled.button`
  ${({ theme, importance = 'primary' }) => `
    position: relative;
    font-family: "Firenight", sans-serif;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    padding: 23px 65px;
    font-size: 36px;
    line-height: 1.2;
    color: ${importance === 'outline' ? theme.color.secondary : 'white'};
    white-space: nowrap;
    transition: color 0.2s ease;

    ${theme.media.mobile} {
      font-size: 22px;
      padding: 13px 36px;
    }
    
    ${Back} {
      stroke-width: 2px;
      fill: ${importance === 'outline' ? 'none' : theme.color.secondary};
      stroke: ${importance === 'outline' ? theme.color.secondary : 'none'};
    }
    
    :hover {
      color: ${importance === 'outline' ? '#FA9326' : ''};
    
      ${Back} {
        fill: ${importance === 'outline' ? 'none' : '#FA9326'};
        stroke: ${importance !== 'outline' ? 'none' : '#FA9326'};
      }
    }
    
    :active {
      color: ${importance === 'outline' ? '#DD3603' : ''};
    
      ${Back} {
        fill: ${importance === 'outline' ? 'none' : '#DD3603'};
        stroke: ${importance !== 'outline' ? 'none' : '#DD3603'};
      }
    }
  `}
`;

export const Back = styled(CustomBack)`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  transition: fill 0.2s ease, stroke 0.2s ease;
`;

export const Text = styled.span`
  position: relative;
`;
