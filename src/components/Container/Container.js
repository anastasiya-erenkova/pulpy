import styled from 'styled-components';

const padding = '36px';

export const Container = styled.div`
  ${({ theme }) => `
    margin-left: auto;
    margin-right: auto;
    max-width: calc(${theme.size.xl} + ${padding} * 2);
    width: 100%;
    padding: 0 ${padding};

    ${theme.media.mobile} {
      padding: 0 18px;
    }
  `}
`;

export default Container;
