import React from 'react';

import { CustomNav, Links, Item, Socials, Phone } from './Nav.styles';

const sortNav = (linkA, linkB) =>
  linkA.OrderNumber >= linkB.OrderNumber ? 1 : -1;

const renderItem = (item) => (
  <Item key={item.id} href={item.Link}>
    {item.Title}
  </Item>
);

const Nav = ({ navigation, phone, ...props }) => (
  <CustomNav {...props}>
    <Links>
      {navigation
        .filter((link) => link.Visible)
        .sort(sortNav)
        .map(renderItem)}
    </Links>
    <Socials />
    <Phone href={`tel:${phone.replace(/[^\d;]/g, '')}`}>{phone}</Phone>
  </CustomNav>
);

export default Nav;
