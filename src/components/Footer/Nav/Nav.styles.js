import styled from 'styled-components';

import hoverLink from '../../../helpers/hoverLink';

import CustomSocials from '../Socials';

export const CustomNav = styled.nav`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Links = styled.div`
  display: flex;
  justify-content: center;

  ${({ theme }) => `
    ${theme.media.tablet} {
      flex-direction: column;
      align-items: center;
    }
  `}
`;

export const Item = styled.a`
  display: inline-block;
  padding: 10px;
  font-size: 36px;
  line-height: 1.2;
  margin-right: 50px;

  :last-child {
    margin-right: 0;
  }

  ${hoverLink}

  ${({ theme }) => `
    ${theme.media.tablet} {
      margin-right: 0;
      margin-bottom: 3px;
      font-size: 28px;
    }
  `}
`;

export const Phone = styled.a`
  display: inline-block;
  padding: 10px;
  font-size: 36px;
  line-height: 1.2;

  ${hoverLink}

  ${({ theme }) => `
    ${theme.media.tablet} {
      font-size: 28px;
    }
  `}
`;

export const Socials = styled(CustomSocials)`
  margin: 50px 0 30px;

  ${({ theme }) => `
    ${theme.media.tablet} {
      margin: 35px 0 25px;
    }
  `}
`;
