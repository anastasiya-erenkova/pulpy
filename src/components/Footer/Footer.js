import React from 'react';

import normalizeText from '../../helpers/normalizeText';
import apiPath from '../../helpers/api';
import Container from '../Container';

import {
  CustomFooter,
  Nav,
  Policy,
  PolicyItem,
  CopyRight,
} from './Footer.styles';

const Footer = ({ docs, navigation, companyInfo }) => (
  <CustomFooter>
    <Container>
      <Nav navigation={navigation} phone={companyInfo.phone} />
      <Policy>
        <PolicyItem href={apiPath + docs.ConfidentialsPdf} target="_blank">
          Положение о конфиденциальности
        </PolicyItem>
        <PolicyItem href={apiPath + docs.TermsPdf} target="_blank">
          Условия пользования веб-сайтом
        </PolicyItem>
      </Policy>
      <CopyRight>{normalizeText(companyInfo.copyright)}</CopyRight>
    </Container>
  </CustomFooter>
);

export default Footer;
