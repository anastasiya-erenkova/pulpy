import styled from 'styled-components';

import CustomNav from './Nav';
import hoverLink from '../../helpers/hoverLink';

export const CustomFooter = styled.footer`
  position: relative;
  z-index: 1000;
  background: linear-gradient(#fa9326 0%, #ffbc41 100%);
  padding: 80px 0 35px;

  ${({ theme }) => `
    ${theme.media.mobile} {
      padding: 40px 0;
    }
  `}
`;

export const Nav = styled(CustomNav)`
  margin-bottom: 18px;

  ${({ theme }) => `
    ${theme.media.tablet} {
      margin-bottom: 12px;
    }
  `}
`;

export const Policy = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 20px;

  ${({ theme }) => `
    ${theme.media.tablet} {
      flex-direction: column;
      align-items: center;
    }
  `}
`;

export const PolicyItem = styled.a`
  display: inline-block;
  padding: 10px;
  font-family: GothamPro;
  font-size: 24px;
  line-height: 1.2;
  letter-spacing: 0.01em;
  margin-right: 30px;

  ${hoverLink}

  :last-child {
    margin-right: 0;
  }

  ${({ theme }) => `
    ${theme.media.tablet} {
      margin-right: 0;
      text-align: center;
      font-size: 16px;
      margin-bottom: 5px;
          
      :last-child {
        margin-bottom: 0;
      }
    }
  `}
`;

export const CopyRight = styled.p`
  text-align: center;
  font-family: GothamPro;
  font-size: 22px;
  line-height: 1.1;
  letter-spacing: 0.01em;
  opacity: 0.6;

  ${({ theme }) => `
    ${theme.media.tablet} {
      font-size: 14px;
      line-height: 1.5;
    }

    ${theme.media.mobile} {
      font-size: 11px;
    }
  `}

  span {
    display: block;
  }
`;
