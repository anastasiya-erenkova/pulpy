import React from 'react';

import IconInst from '../../icons/Instagram';
import IconYouTube from '../../icons/Youtube';
import IconVk from '../../icons/Vk';

import { SocialsContainer, Youtube, Vk, Instagram } from './Socials.styles';

const Socials = ({ ...props }) => (
  <SocialsContainer {...props}>
    <Instagram href="https://www.instagram.com/pulpyshake/" target="_blank">
      <IconInst />
    </Instagram>
    <Youtube
      href="https://www.youtube.com/channel/UCOAahYfvYOw6_Ojvsdo00mQ"
      target="_blank"
    >
      <IconYouTube />
    </Youtube>
    <Vk href="https://vk.com/pulpyshake" target="_blank">
      <IconVk />
    </Vk>
  </SocialsContainer>
);

export default Socials;
