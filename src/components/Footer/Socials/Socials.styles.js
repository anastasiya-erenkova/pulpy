import styled from 'styled-components';

export const SocialsContainer = styled.div`
  display: flex;
`;

export const Social = styled.a`
  border-radius: 50%;
  width: 138px;
  height: 138px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: background 0.2s ease;

  :not(:last-child) {
    margin-right: 35px;
  }

  ${({ theme }) => `
    ${theme.media.tablet} {
      width: 64px;
      height: 64px;
      
      :not(:last-child) {
        margin-right: 15px;
      }
    }
    
    svg {
      fill: white;
    }
  `}
`;

export const Instagram = styled(Social)`
  background-color: #9b51e0;

  :hover {
    background-color: #cc51e0;
  }

  svg {
    width: 39px;
    height: 39px;

    ${({ theme }) => `
      ${theme.media.tablet} {
        width: 26px;
        height: 26px;
      }
    `}
  }
`;

export const Youtube = styled(Social)`
  background-color: #dd3603;

  :hover {
    background-color: #dd0303;
  }

  svg {
    width: 42px;
    height: 30px;

    ${({ theme }) => `
      ${theme.media.tablet} {
        width: 32px;
        height: 22px;
      }
    `}
  }
`;

export const Vk = styled(Social)`
  background-color: #2b8bc7;

  :hover {
    background-color: #2b57c7;
  }

  svg {
    width: 47px;
    height: 27px;

    ${({ theme }) => `
      ${theme.media.tablet} {
        width: 30px;
        height: 17px;
      }
    `}
  }
`;
