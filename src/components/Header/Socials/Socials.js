import React from 'react';

import IconInst from '../../icons/Instagram';
import IconYouTube from '../../icons/Youtube';
import IconVk from '../../icons/Vk';

import { SocialsContainer, Youtube, Vk, Instagramm } from './Socials.styles';

const Socials = ({ ...props }) => (
  <SocialsContainer {...props}>
    <Youtube
      href="https://www.youtube.com/channel/UCOAahYfvYOw6_Ojvsdo00mQ"
      target="_blank"
    >
      <IconYouTube />
    </Youtube>
    <Vk href="https://vk.com/pulpyshake" target="_blank">
      <IconVk />
    </Vk>
    <Instagramm href="https://www.instagram.com/pulpyshake/" target="_blank">
      <IconInst />
    </Instagramm>
  </SocialsContainer>
);

export default Socials;
