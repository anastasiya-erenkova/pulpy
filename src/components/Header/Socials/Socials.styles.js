import styled from 'styled-components';

export const SocialsContainer = styled.div`
  display: flex;
`;

export const Social = styled.a`
  background-color: white;
  border-radius: 50%;
  width: 55px;
  height: 55px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: 0.2s ease;

  :not(:last-child) {
    margin-right: 20px;
  }

  ${({ theme }) => `
    ${theme.media.tablet} {
      width: 64px;
      height: 64px;
    }
    
    :hover {
      background-color: ${theme.color.secondary};
      
      svg {
        fill: white;
      }
    }
    
    svg {
      transition: fill 0.25s ease;
      fill: ${theme.color.secondary};
    }
  `}
`;

export const Youtube = styled(Social)`
  svg {
    width: 25px;
    height: 18px;

    ${({ theme }) => `
      ${theme.media.tablet} {
        width: 30px;
        height: 21px;
      }
    `}
  }
`;

export const Vk = styled(Social)`
  svg {
    width: 24px;
    height: 14px;

    ${({ theme }) => `
      ${theme.media.tablet} {
        width: 29px;
        height: 17px;
      }
    `}
  }
`;

export const Instagramm = styled(Social)`
  svg {
    width: 24px;
    height: 24px;

    ${({ theme }) => `
      ${theme.media.tablet} {
        width: 29px;
        height: 29px;
      }
    `}
  }
`;
