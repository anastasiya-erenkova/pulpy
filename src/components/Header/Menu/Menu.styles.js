import styled from 'styled-components';

import hoverLink from '../../../helpers/hoverLink';

import CustomSocials from '../Socials';

const bottom = '50px';

export const CustomMenu = styled.div`
  ${({ theme, isScroll }) => `
    ${theme.media.tablet} {
      ${Content} {
        transition: transform 0.3s ease;
        transform: translateY(${isScroll ? '-35px' : '0'});
      }
    }
  `}
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Button = styled.button.attrs({ type: 'button' })`
  ${({ theme }) => `
    position: relative;

    ::before {
      content: '';
      display: inline-block;
      width: 55px;
      height: 55px;
      border-radius: 50%;
      background-color: white;
      transition: 0.2s ease;

      ${theme.media.tablet} {
        width: 40px;
        height: 40px;
      }
    }
    
    ${theme.media.tablet} {
      padding: 10px;
    }
    
    svg {
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      transition: 0.2s ease;
      fill: ${theme.color.secondary};
    }
    
    :hover {
      ::before {
        background-color: ${theme.color.secondary};
      }
      
      svg {
        fill: white;
      }
    }
  `}
`;

export const Open = styled(Button)`
  svg {
    width: 30px;
    height: 23px;

    ${({ theme }) => `
      ${theme.media.tablet} {
        width: 22px;
        height: 17px;
        top: 48%;
      }
  `}
  }
`;

export const Close = styled(Button)`
  svg {
    width: 24px;
    height: 24px;

    ${({ theme }) => `
      ${theme.media.tablet} {
        width: 19px;
        height: 19px;
      }
  `}
  }
`;

export const Hint = styled.span`
  margin-top: 12px;
  font-size: 20px;
  letter-spacing: 0.05em;

  ${({ theme }) => `
    ${theme.media.tablet} {
      display: none;
    }
  `}
`;

export const OpenContent = styled(Content)`
  ${Hint} {
    transition: 0.2s ease;
  }
`;

export const Sidebar = styled.div`
  ${({ theme, isOpen }) => `
    background: ${theme.linear.sidebar};
    position: fixed;
    left: 0;
    top: 0;
    bottom: 0;
    width: 590px;
    transition: 0.5s ease;
    transform: translateX(${isOpen ? '0' : '-100%'});

    ${theme.media.tablet} {
      width: auto;
      right: 0;
      transition: 0.3s ease;
    }
  `}
`;

export const CloseContent = styled(Content)`
  position: absolute;
  top: 45px;
  left: 43px;

  ${({ theme }) => `
    ${theme.media.tablet} {
      left: auto;
      right: 9px;
      top: 52px;
    }
  `}
`;

export const Nav = styled.nav`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-bottom: calc(${bottom} + 50px);
  padding-top: 14%;

  ${({ theme }) => `
    ${theme.media.tablet} {
      padding: 130px 0 50px;
    }
  `}
`;

export const NavItem = styled.a`
  padding: 10px;
  font-size: 48px;
  line-height: 1.2;

  :not(:last-child) {
    margin-bottom: 40px;
  }

  ${hoverLink}

  ${({ theme }) => `
    ${theme.media.tablet} {
      font-size: 28px;

      :not(:last-child) {
        margin-bottom: 2px;
      }
    }
  `}
`;

export const Telephone = styled.a`
  ${hoverLink}
  position: absolute;
  bottom: ${bottom};
  left: 50%;
  transform: translateX(-50%);
  font-size: 36px;
  line-height: 1.2;
  padding: 10px;

  ${({ theme }) => `
    ${theme.media.tablet} {
      position: static;
      font-size: 28px;
      transform: translateX(0);
    }
  `}
`;

export const Socials = styled(CustomSocials)`
  display: none;

  ${({ theme }) => `
    ${theme.media.tablet} {
      display: inherit;
      margin: 33px 0 25px;
    }
  `}
`;
