import React, { useState } from 'react';

import {
  CustomMenu,
  OpenContent,
  Open,
  Hint,
  Sidebar,
  CloseContent,
  Close,
  Nav,
  NavItem,
  Telephone,
  Socials,
} from './Menu.styles';

const sortNav = (linkA, linkB) =>
  linkA.OrderNumber >= linkB.OrderNumber ? 1 : -1;

const Menu = ({ isScroll, navigation, phone, ...props }) => {
  const [isOpen, switchOpen] = useState(false);

  const onClose = () => switchOpen(false);

  const renderNavItem = (item) => (
    <NavItem key={item.id} href={item.Link} onClick={onClose}>
      {item.Title}
    </NavItem>
  );

  return (
    <CustomMenu isScroll={isScroll} {...props}>
      <OpenContent>
        <Open onClick={() => switchOpen(true)}>
          <svg
            viewBox="0 0 44 33"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M0 28.0851L44 26.6809L42.642 32.2979L1.35802 33L0 28.0851Z" />
            <path d="M0 14.7447L44 13.3404L42.642 18.9574L1.35802 19.6596L0 14.7447Z" />
            <path d="M0 1.40426L44 0L42.642 5.61702L1.35802 6.31915L0 1.40426Z" />
          </svg>
        </Open>
        <Hint>Меню</Hint>
      </OpenContent>
      <Sidebar isOpen={isOpen}>
        <CloseContent>
          <Close onClick={onClose}>
            <svg
              viewBox="0 0 36 36"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M34.7976 3.84444L4.67783 35.9501L1.66626 31.018L30.3619 1.32936L34.7976 3.84444Z" />
              <path d="M3.68478 1.36198L35.7904 31.4817L30.8583 34.4933L1.16969 5.7976L3.68478 1.36198Z" />
            </svg>
          </Close>
          <Hint>Свернуть</Hint>
        </CloseContent>
        <Nav>
          {navigation
            .filter((link) => link.Visible)
            .sort(sortNav)
            .map(renderNavItem)}
          <Socials />
          <Telephone href={`tel:${phone.replace(/[^\d;]/g, '')}`}>
            {phone}
          </Telephone>
        </Nav>
      </Sidebar>
    </CustomMenu>
  );
};

export default Menu;
