import React, { useState, useEffect } from 'react';

import sections from '../../helpers/sections';

import logo from './logo.png';
import { CustomHeader, Wrap, Menu, Logo, Socials } from './Header.styles';

const Header = ({ navigation, companyInfo }) => {
  const [isScroll, setIsScroll] = useState(false);

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 40) {
        setIsScroll(true);
      } else {
        setIsScroll(false);
      }
    });
  }, []);

  return (
    <CustomHeader>
      <Wrap>
        <Menu
          isScroll={isScroll}
          navigation={navigation}
          phone={companyInfo.phone}
        />
      </Wrap>
      <Socials />
      <Logo href={`#${sections.hero}`} isScroll={isScroll}>
        <img src={logo} alt="Pulpy" />
      </Logo>
    </CustomHeader>
  );
};

export default Header;
