import styled from 'styled-components';

import CustomMenu from './Menu';
import CustomSocials from './Socials';

export const CustomHeader = styled.header`
  position: relative;
`;

export const Wrap = styled.div`
  ${({ theme }) => `
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 5000;
    display: flex;
    justify-content: space-between;
    padding: 45px 43px 0;

    ${theme.media.tablet} {
      flex-flow: row-reverse;
      padding: 52px 9px 0;
    }
  `}
`;

export const Menu = styled(CustomMenu)``;

export const Logo = styled.a`
  ${({ theme, isScroll }) => `
    position: absolute;
    left: 50%;
    top: 45px;
    transform: translateX(-50%);
    width: 398px;
    transition: 0.2s ease;
    z-index: 5000;
    cursor: default;
    
    ${theme.media.desktop} {
      width: 250px;
    }

    ${theme.media.tablet} {
      position: fixed;
      width: 123px;
      left: 16px;
      top: 52px;
      transform: translate(0, 8%);
      transition: transform 0.3s ease;
      transform: translateY(${isScroll ? '-30px' : '8%'});
      cursor: pointer;
    }

    img {
      max-width: 100%
    }
  `}
`;

export const Socials = styled(CustomSocials)`
  position: absolute;
  right: 43px;
  top: 45px;
  z-index: 5000;

  ${({ theme }) => `
    ${theme.media.tablet} {
      display: none;
    }
  `}
`;
