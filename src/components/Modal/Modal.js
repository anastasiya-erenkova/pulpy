import { useEffect } from 'react';
import ReactDOM from 'react-dom';

const body = document.querySelector('body');

const Modal = ({ children, className, id }) => {
  const modal = document.createElement('div');

  useEffect(() => {
    body.appendChild(modal);
    className.split(' ').forEach((clazz) => modal.classList.add(clazz));

    if (id) {
      modal.id = id;
    }

    return () => {
      body.removeChild(modal);
      modal.removeAttribute('class');
    };
  });

  return ReactDOM.createPortal(children, modal);
};

export default Modal;
