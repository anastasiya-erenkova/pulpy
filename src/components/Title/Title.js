import styled from 'styled-components';

const Title = styled.h2`
  font-size: 100px;
  line-height: 1.2;

  ${({ theme }) => `
    ${theme.media.desktop} {
      font-size: 80px;
    }
    
    ${theme.media.mobile} {
      font-size: 36px;
    }
  `}
`;

export default Title;
