import React, { useEffect, useState } from 'react';

import apiPath from '../../helpers/api';
import addScroll from '../../helpers/scroll';
import Header from '../../components/Header';
import Hero from '../../page_elements/Hero';
import About from '../../page_elements/About';
import Products from '../../page_elements/Products';
import Promos from '../../page_elements/Promos';
import Gif from '../../page_elements/Gif';
import Footer from '../../components/Footer';

import { CustomMain, Preloader, PreloaderInner } from './Main.styles';
import logo from './logo.png';

const Main = () => {
  const [resultFetch, setResultFetch] = useState(false);
  const [heroLoading, setHeroLoading] = useState(true);
  const [loading, setLoading] = useState(true);
  const [percent, setPercent] = useState(0);
  const [docs, setDocs] = useState({});
  const [logoImg, setLogoImg] = useState({});
  const [logoImgMob, setLogoImgMob] = useState({});
  const [products, setProducts] = useState([]);
  const [promos, setPromos] = useState([]);
  const [promosProducts, setPromosProducts] = useState([]);
  const [gif, setGif] = useState({});
  const [navigation, setNavigation] = useState({});
  const [companyInfo, setCompanyInfo] = useState({});

  const setPreloader = () => {
    setPercent((percent) => {
      const newPercent = percent + Math.ceil(100 / (fetchList.length + 1));
      return newPercent < 100 ? newPercent : 100;
    });
  };

  const fetchDocs = async () => {
    const result = await fetch(`${apiPath}/terms-and-conditions`);
    if (result.ok) {
      const data = await result.json();
      setDocs(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchLogoImg = async () => {
    const result = await fetch(`${apiPath}/main-logo-layers`);
    if (result.ok) {
      const data = await result.json();
      setLogoImg(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchLogoImgMob = async () => {
    const result = await fetch(`${apiPath}/main-logo-mobile-layers`);
    if (result.ok) {
      const data = await result.json();
      setLogoImgMob(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchProducts = async () => {
    const result = await fetch(`${apiPath}/products`);
    if (result.ok) {
      const data = await result.json();
      setProducts(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchPromos = async () => {
    const result = await fetch(`${apiPath}/promo-cards`);
    if (result.ok) {
      const data = await result.json();
      setPromos(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchPromosProducts = async () => {
    const result = await fetch(`${apiPath}/product-cards`);
    if (result.ok) {
      const data = await result.json();
      setPromosProducts(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchGif = async () => {
    const result = await fetch(`${apiPath}/promo-gif`);
    if (result.ok) {
      const data = await result.json();
      setGif(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchNavigation = async () => {
    const result = await fetch(`${apiPath}/menu-items`);
    if (result.ok) {
      const data = await result.json();
      setNavigation(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchCompanyInfo = async () => {
    const result = await fetch(`${apiPath}/company-info`);
    if (result.ok) {
      const data = await result.json();
      setCompanyInfo(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const loadingHeroImages = () => {
    setHeroLoading(false);
    setPreloader();
  };

  const fetchList = [
    fetchDocs,
    fetchLogoImg,
    fetchLogoImgMob,
    fetchProducts,
    fetchPromos,
    fetchPromosProducts,
    fetchGif,
    fetchNavigation,
    fetchCompanyInfo,
  ];

  const fetchAll = () => {
    async function fetchData() {
      window.scrollTo(0, 0);

      const result = await Promise.all(
        fetchList.map((func) => func()),
      ).then((values) => values.every((val) => val));
      if (result) {
        setResultFetch(true);
      } else {
        console.error('error');
      }
    }

    fetchData();
  };

  useEffect(fetchAll, []);

  useEffect(() => {
    if (!heroLoading && resultFetch) {
      window.scrollTo(0, 0);
      setLoading(false);
      addScroll();
      setTimeout(() => {
        document.querySelector('#loader').classList.add('hide');
        if (window.location.hash) {
          const element = document.getElementById(
            window.location.hash.replace('#', ''),
          );
          element && element.scrollIntoView({ behavior: 'smooth' });
        }
      }, 1150);
      setTimeout(() => {
        document.parallaxFunc();
      }, 9000);
    }
  }, [heroLoading, resultFetch]);

  return (
    <>
      <Preloader loading={loading} id="loader">
        <PreloaderInner>
          <span>{percent}%</span>
          <img src={logo} alt="" />
        </PreloaderInner>
      </Preloader>
      {resultFetch && percent === 100 && (
        <Header navigation={navigation} companyInfo={companyInfo} />
      )}
      <CustomMain>
        {resultFetch && percent > 85 && (
          <Hero
            img={logoImg}
            imgMob={logoImgMob}
            setHeroLoading={loadingHeroImages}
            loading={loading}
          />
        )}
        {resultFetch && percent === 100 && (
          <>
            <About />
            <Products products={products} />
            <Promos promos={promos} products={promosProducts} />
            {!!gif.Enabled && <Gif gif={gif} />}
          </>
        )}
      </CustomMain>
      {resultFetch && percent === 100 && (
        <Footer docs={docs} navigation={navigation} companyInfo={companyInfo} />
      )}
    </>
  );
};

export default Main;
