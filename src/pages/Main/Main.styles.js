import styled from 'styled-components';

import shineImg from './shine.png';

export const CustomMain = styled.main`
  background-color: #76c0bd;
`;

export const PreloaderInner = styled.div`
  position: absolute;
  top: calc(50% - 115px);
  left: 0;
  font-size: 80px;
  width: 230px;
  height: 230px;
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    position: relative;
    z-index: 10;
  }

  img {
    position: absolute;
    width: 170px;
    top: -35px;
    left: calc(50% - 85px);
    z-index: 15;
    animation: logo ease 3s infinite;
  }

  ::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    animation: circle linear 3s infinite;
    width: 230px;
    height: 230px;
    border-radius: 50%;
    background: rgb(33, 144, 207);
    background: linear-gradient(
      45deg,
      rgba(33, 144, 207, 1) 0%,
      rgba(54, 168, 224, 1) 25%,
      rgba(105, 180, 227, 1) 100%
    );
  }

  ::before {
    content: '';
    position: absolute;
    top: 50%;
    left: 50%;
    animation: shine linear 3s infinite;
    background-image: url(${shineImg});
    width: 220%;
    height: 220%;
    max-width: 500px;
    max-height: 500px;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
  }

  @keyframes circle {
    0% {
      transform: scale(1);
    }

    50% {
      transform: scale(1.2);
    }

    100% {
      transform: scale(1);
    }
  }

  @keyframes logo {
    0% {
      transform: scale(1.1);
    }

    50% {
      transform: scale(0.8);
    }

    100% {
      transform: scale(1.1);
    }
  }

  @keyframes shine {
    0% {
      transform: translate(-50%, -50%) scale(1) rotate(0deg);
    }

    40% {
      transform: translate(-50%, -50%) scale(1.5) rotate(150deg);
    }

    100% {
      transform: translate(-50%, -50%) scale(1) rotate(360deg);
    }
  }
`;

export const Preloader = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  width: 230px;
  max-width: 1152px;
  margin: 0 auto;
  height: ${document.documentElement.clientHeight}px;
  transition: all 0.5s ease;
  z-index: 999999999999;
  ${({ loading }) =>
    !loading && window.innerWidth >= 768
      ? 'animation: wrapMain 1.2s ease 1'
      : ''};
  animation-fill-mode: forwards;
  
  &.hide {
    opacity: 0;
    visibility: hidden;
  }
  
  ::before {
    content: '';
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: ${({ theme }) => theme.color.primary};
  }

  ${PreloaderInner} {
    ${({ loading }) =>
      !loading && window.innerWidth >= 768
        ? 'animation: wrap 1.2s ease 1'
        : ''};
    animation-fill-mode: forwards;
    
    ::after {
      ${({ loading }) =>
        !loading && window.innerWidth >= 768
          ? 'animation: scale 1.2s ease 1'
          : ''};
      animation-fill-mode: forwards;
        }
        
    ::before {
      ${({ loading }) =>
        !loading && window.innerWidth >= 768
          ? 'animation: shining 1.2s ease 1'
          : ''};
      animation-fill-mode: forwards;
    }

    img {
      ${({ loading }) =>
        !loading && window.innerWidth >= 768
          ? 'animation: logoUp 1.2s ease 1'
          : ''};
      animation-fill-mode: forwards;
    }

    span {
      transition: 0.3s ease;
      ${({ loading }) =>
        !loading && window.innerWidth >= 768 ? 'opacity: 0;' : ''}
    }
  }

  @keyframes wrap {
    0% {
    }
    100% {
      height: 58vw;
      max-height: 1160px;
      width: 58vw;
      max-width: 1160px;
      top: 0;
      left: 0;
    }
  }
}

@keyframes wrapMain {
  0% {
  }
  100% {
    max-width: 1160px;
    width: 58vw;
    }
  }
  
  @keyframes scale {
    0% {
    top: -13%;
    }
    20% {
    top: -13%;
    }
    70% {
    top: -13%;
    }
    100% {
      top: -13%;
      left: 0;
      width: 58vw;
      height: 58vw;
      max-width: 1160px;
      max-height: 1160px;
    }
  }

  @keyframes logoUp {
    0% {
    }
    100% {
      top: 45px;
      ${
        window.innerWidth < 1440
          ? `width: 250px;
            left: calc(50% - 125px);`
          : `left: calc(50% - 200px);
             width: 398px;`
      }
    }
  }

  @keyframes shining {
    0% {
      opacity: 0;
    }
    
    100% {
      opacity: 0;
    }
  }
`;
