import React, { useEffect, useState } from 'react';

import apiPath from '../../helpers/api';
import Header from '../../components/Header';
import image from './not-found.png';

import {
  CustomMain,
  Preloader,
  PreloaderInner,
  Image,
  Text,
  Description,
  Title,
} from './NotFound.styles';
import logo from './logo.png';
import Button from '../../components/Button';

const NotFound = () => {
  const [resultFetch, setResultFetch] = useState(false);
  const [loading, setLoading] = useState(true);
  const [percent, setPercent] = useState(0);
  const [navigation, setNavigation] = useState({});
  const [companyInfo, setCompanyInfo] = useState({});

  const setPreloader = () => {
    setPercent((percent) => {
      const newPercent = percent + Math.ceil(100 / fetchList.length);
      return newPercent < 100 ? newPercent : 100;
    });
  };

  const fetchNavigation = async () => {
    const result = await fetch(`${apiPath}/menu-items`);
    if (result.ok) {
      const data = await result.json();
      setNavigation(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchCompanyInfo = async () => {
    const result = await fetch(`${apiPath}/company-info`);
    if (result.ok) {
      const data = await result.json();
      setCompanyInfo(data);
      setTimeout(setPreloader, 1000);
      return true;
    }
    return false;
  };

  const fetchList = [fetchNavigation, fetchCompanyInfo];

  const fetchAll = () => {
    async function fetchData() {
      const result = await Promise.all(
        fetchList.map((func) => func()),
      ).then((values) => values.every((val) => val));
      if (result) {
        setResultFetch(true);
      } else {
        console.error('error');
      }
    }

    fetchData();
  };

  useEffect(fetchAll, []);

  useEffect(() => {
    if (resultFetch) {
      setLoading(false);
      setTimeout(() => {
        document.querySelector('#loader').classList.add('hide');
      }, 1000);
    }
  }, [resultFetch]);

  return (
    <>
      <Preloader loading={loading} id="loader">
        <PreloaderInner>
          <span>{percent}%</span>
          <img src={logo} alt="" />
        </PreloaderInner>
      </Preloader>
      {resultFetch && percent > 95 && (
        <Header navigation={navigation} companyInfo={companyInfo} />
      )}
      <CustomMain>
        <Image>
          <img src={image} alt="Not found" />
        </Image>
        <Text>
          <Title>Страница не найдена</Title>
          <Description>
            Страница, которую вы ищете, не существует... Вы можете вернуться
            назад или перейти на главную страницу
          </Description>
          <Button as="a" href="/">
            На главную
          </Button>
        </Text>
      </CustomMain>
    </>
  );
};

export default NotFound;
