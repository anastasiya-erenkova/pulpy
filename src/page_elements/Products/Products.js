import React from 'react';

import sections from '../../helpers/sections';
import Container from '../../components/Container';

import Product from './Product';

import {
  Section,
  Title,
  Description,
  ProductsList,
  Id,
} from './Products.styles';

const renderProduct = (product) => (
  <Product key={product.id} product={product} />
);

const sortProducts = (prodA, prodB) =>
  prodA.orderNumber >= prodB.orderNumber ? -1 : 1;

const Products = ({ products }) => (
  <Section>
    <Id id={sections.products} />
    <Container>
      <Title>Выбери свой pulpy</Title>
      <Description>
        Pulpy продается в своей фирменной бутылке в объемах 0,45л. и 0,9л.,
        верхняя часть которой сделана подобно апельсиновой корочке
      </Description>
      <ProductsList>
        {!!products.length &&
          products
            .filter((p) => p.IsVisible)
            .sort(sortProducts)
            .map(renderProduct)}
      </ProductsList>
    </Container>
  </Section>
);

export default Products;
