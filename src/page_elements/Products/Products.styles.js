import styled from 'styled-components';

import CustomTitle from '../../components/Title';
import CustomDescription from '../../components/Description';

import { Item as Product } from './Product';

export const Section = styled.section`
  ${({ theme }) => `
    position: relative;
    padding: 122px 0;
    background: ${theme.linear.primary};
    position: relative;
    z-index: 100;
    text-align: center;

    ${theme.media.mobile} {
      padding: 50px 0 20px;
    }
  `}
`;

export const Id = styled.span`
  ${({ theme }) => `
    position: absolute;
    display: block;
    top: -120px;

    ${theme.media.mobile} {
      top: -50px;
    }
  `}
`;

export const Title = styled(CustomTitle)`
  margin-bottom: 18px;
`;

export const Description = styled(CustomDescription)`
  max-width: 930px;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 30px;
`;

export const ProductsList = styled.div`
  ${({ theme }) => `
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin-left: -10px;
    margin-right: -10px;

    ${theme.media.desktop} {
      margin-left: 0px;
      margin-right: 0px;
    }

    ${Product} {
      flex-basis: 40%;
    
      :nth-child(odd) {
        margin-right: 10%;
      }
      
      :last-child {
        margin-right: 0;
      }

      ${theme.media.tablet} {
        flex-basis: 100%;
        
        :nth-child(odd) {
          margin-right: 0;
        }
      }
    }
  `}
`;
