import styled from 'styled-components';

const marginDeskXL = '115px';
const marginDesk = '90px';
const marginDeskSM = '80px';
const marginMob = '70px';

export const ImageLayer = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  transition: transform 0.2s ease;

  :nth-child(2) {
    z-index: 2;
  }
  :nth-child(3) {
    z-index: 3;
  }
  :nth-child(4) {
    z-index: 4;
  }
`;

export const Item = styled.a`
  position: relative;
  display: block;
  margin-bottom: 50px;
  transition: 1s ease;

  :hover {
    ${ImageLayer}:nth-child(3) {
      transform: rotate(15deg) scale(1.1);
    }

    ${ImageLayer}:nth-child(4) {
      transform: scale(1.1);
    }

    span::after {
      transform: scale(1);
    }
  }
`;

export const Title = styled.p`
  ${({ theme }) => `
    position: relative;
    top: -${marginDeskXL};
    margin-bottom: -${marginDeskXL};
    font-size: 36px;
    line-height: 1.2;
    z-index: 10;
    display: flex;
    flex-direction: column;
    align-items: center;

    ${theme.media.desktop} {
      top: -${marginDesk};
      margin-bottom: -${marginDesk};
      font-size: 30px;
    }
    
    @media (max-width: 1200px) {
      top: -${marginDeskSM};
      margin-bottom: -${marginDeskSM};
    }
    
    ${theme.media.tablet} {
      top: -${marginDesk};
      margin-bottom: -${marginDesk};
    }

    ${theme.media.mobile} {
      font-size: 24px;
      padding: 0 18px;
      top: -${marginMob};
      margin-bottom: -${marginMob};
    }
  `}

  span {
    display: inline-block;
    position: relative;

    ::after {
      content: '';
      position: absolute;
      left: 0;
      bottom: 0;
      width: 100%;
      height: 2px;
      background-color: white;
      transform: scale(0);
      transition: transform 0.25s ease;
    }
  }
`;

export const ImageWrap = styled.div`
  ${({ theme }) => `
    max-width: 100%;
    position: relative;
    margin: 0 auto;

    ${theme.media.desktop} {
      width: 100%;
      max-width: 475px;
    }
    
    @media (max-width: 1200px) {
      max-width: 385px;
    }
    
    ${theme.media.tablet} {
      max-width: 475px;
    }

    ${theme.media.mobile} {
      width: 340px;
    }
  `}
`;

export const Image = styled.img`
  max-width: 100%;
  z-index: 0;
`;

export const NewLabel = styled.img`
  ${({ theme }) => `
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    top: 485px;
    width: 290px;
    z-index: 5;

    ${theme.media.desktop} {
      top: 380px;
      width: 240px;
    }
    
    @media (max-width: 1200px) {
      top: 302px;
      width: 210px;
    }
    
    ${theme.media.tablet} {
      top: 380px;
      width: 240px;
    }

    ${theme.media.mobile} {
      top: 285px;
      width: 154px;
    }
  `}
`;
