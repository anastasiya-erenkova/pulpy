import React from 'react';

import apiPath from '../../../helpers/api';
import sections from '../../../helpers/sections';
import normalizeName from '../../../helpers/normalizeText';
import ModalProduct from '../../ModalProduct';

import newLabel from './new-label.png';
import {
  Item,
  Title,
  Image,
  NewLabel,
  ImageWrap,
  ImageLayer,
} from './Product.styles';

function calcScroll() {
  const div = document.createElement('div');
  div.style.overflowY = 'scroll';
  div.style.width = '50px';
  div.style.height = '50px';
  div.style.visibility = 'hidden';

  document.body.appendChild(div);
  const scrollWidth = div.offsetWidth - div.clientWidth;
  document.body.removeChild(div);

  return scrollWidth;
}

const scrollWidth = calcScroll();

const Product = ({ product, ...props }) => {
  if (!product || !product.id) return null;

  const openModal = () => {
    const modal = document.querySelector(`#product-modal-${product.id}`);
    const gif = document.querySelector(`#${sections.gif}`);
    document.querySelector('html').classList.add('fixed');
    document.querySelector('html').style.marginRight = `${scrollWidth}px`;
    if (gif) {
      gif.style.marginRight = `${scrollWidth}px`;
    }
    modal.classList.add('open');
  };
  const closeModal = () => {
    const modal = document.querySelector(`#product-modal-${product.id}`);
    const gif = document.querySelector(`#${sections.gif}`);
    modal.classList.remove('open');
    setTimeout(() => {
      document.querySelector('html').classList.remove('fixed');
      document.querySelector('html').style.marginRight = '';
      if (gif) {
        gif.style.marginRight = '';
      }
    }, 300);
  };

  const {
    id,
    Name,
    IsNewProduct,
    InfoBackground,
    Z1_Image,
    Z2_Image,
    Z3_Image,
    Z4_Image,
  } = product;

  return (
    <>
      <Item {...props} id={`product-${id}`} onClick={openModal}>
        {Z1_Image && (
          <ImageWrap>
            <Image src={apiPath + Z1_Image.url} alt="" />
            <ImageLayer src={apiPath + Z2_Image.url} alt="" />
            <ImageLayer src={apiPath + Z3_Image.url} alt="" />
            <ImageLayer src={apiPath + Z4_Image.url} alt="" />
          </ImageWrap>
        )}
        {IsNewProduct && <NewLabel src={newLabel} alt="new" />}
        <Title>{normalizeName(Name)}</Title>
      </Item>
      {InfoBackground && (
        <ModalProduct product={product} onClose={closeModal} />
      )}
    </>
  );
};

export default Product;
