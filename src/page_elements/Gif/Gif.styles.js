import styled from 'styled-components';

export const Container = styled.a`
  ${({ theme, isVisible }) => `
    position: fixed;
    bottom: 50px;
    right: 50px;
    border-radius: 50%;
    width: 100px;
    height: 100px;
    display: block;
    z-index: 2000;
    transition: opacity 0.2s ease, visibility 0.2s ease, transform 0.2s ease ;
    opacity: ${isVisible ? '1' : '0'};
    visibility: ${isVisible ? 'visible' : 'hidden'};

    ${theme.media.tablet} {
      width: 82px;
      height: 82px;
      bottom: 25px;
      right: 25px;
    }
    
    :hover {
      transform: scale(1.2);
      
      ${Dymanic} {
        opacity: 1;
      }
  
      ${Static} {
        opacity: 0;
      }
    }
  `}
`;

const Image = styled.img`
  position: absolute;
  border-radius: 50%;
  height: 100%;
  width: 100%;
`;

export const Static = styled(Image)``;

export const Dymanic = styled(Image)`
  opacity: 0;
  z-index: 2000;
`;

export const Text = styled.img`
  position: absolute;
  height: 130%;
  width: 130%;
  top: -15%;
  left: -15%;
  animation: textMove 25s infinite linear;

  @keyframes textMove {
    0% {
      transform: rotate(360deg);
    }

    100% {
      transform: rotate(0deg);
    }
  }
`;

export const Label = styled.img`
  position: absolute;
  max-width: 85%;
  left: 50%;
  bottom: 8%;
  transform: translateX(-50%);
  z-index: 3000;
`;
