import React, { useState, useEffect } from 'react';

import apiPath from '../../helpers/api';
import sections from '../../helpers/sections';

import { Container, Static, Dymanic, Label, Text } from './Gif.styles';

const Gif = ({ gif }) => {
  const [isVisible, setVisible] = useState(true);

  useEffect(() => {
    document.addEventListener('scroll', () => {
      const el = document.querySelector(`#${sections.promo}`);

      if (
        el &&
        el.getBoundingClientRect().height * 0.8 -
          el.getBoundingClientRect().top >
          0
      ) {
        setVisible(false);
      } else {
        setVisible(true);
      }
    });
  }, []);

  if (!gif) return null;

  const { StaticImage, GifImage, Sticker, SurroundCircle } = gif;

  return StaticImage ? (
    <Container
      href={`#${sections.promo}`}
      isVisible={isVisible}
      id={sections.gif}
    >
      <Static src={apiPath + StaticImage.url} alt="" />
      <Dymanic src={apiPath + GifImage.url} alt="" />
      {Sticker && <Label src={apiPath + Sticker.url} alt="" />}
      <Text src={apiPath + SurroundCircle.url} alt="" />
    </Container>
  ) : null;
};

export default Gif;
