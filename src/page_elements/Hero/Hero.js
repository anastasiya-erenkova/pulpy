import React, { useState, useEffect } from 'react';

import apiPath from '../../helpers/api';
import sections from '../../helpers/sections';

import {
  Section,
  ImageWrap,
  ImageLayers,
  ImageLayersMob,
  ImageDesk,
  ImageMob,
  Scroll,
  BottlesWrap,
} from './Hero.styles';

const renderScroll = () => (
  <Scroll href={`#${sections.about}`}>
    <svg viewBox="0 0 38 22" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M3.00195 3.00012L19.002 19.0001L35.002 3.00012"
        stroke="white"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  </Scroll>
);

const Hero = ({ img, imgMob, setHeroLoading, loading }) => {
  const [imgLoad, setImgLoad] = useState(false);
  const [imgMobLoad, setImgMobLoad] = useState(false);
  const [device, setDevice] = useState(null);
  const [isVisible, setVisible] = useState(false);

  const defineDevice = () => {
    if (window.innerWidth < 768) {
      setDevice('mobile');
    } else {
      setDevice('desktop');
    }
  };

  const classForSection = () => {
    const section = document.querySelector(`#${sections.hero}`);
    if (!section.classList.contains('finish-animation')) {
      section.classList.add('finish-animation');
    }
  };

  useEffect(() => {
    if (imgLoad || imgMobLoad) {
      setHeroLoading && setHeroLoading();
    }
  }, [imgLoad, imgMobLoad, setHeroLoading]);

  useEffect(() => {
    defineDevice();
    window.addEventListener('resize', defineDevice);
    window.addEventListener('resize', classForSection);
  }, []);

  useEffect(() => {
    if (loading) {
      setTimeout(() => {
        setVisible(true);
      }, 1800);

      setTimeout(() => {
        document
          .querySelector(`#${sections.hero}`)
          .classList.add('finish-animation');
      }, 5000);
    }
  }, [loading]);

  return (
    <Section id={sections.hero} isVisible={isVisible}>
      {device === 'desktop' && img.Z1_Image && (
        <ImageWrap device="desktop">
          <ImageDesk
            src={apiPath + img.Z1_Image.url}
            alt="Знакомьтесь, pulpy!"
            onLoad={() => setImgLoad(true)}
          />
          <ImageLayers
            className="HeroImageZ2"
            src={apiPath + img.Z2_Image.url}
            alt=""
          />
          <BottlesWrap>
            <ImageLayers
              className="HeroImageZ3"
              src={apiPath + img.Z3_Image.url}
              alt=""
            />
            <ImageLayers
              className="HeroImageZ5"
              src={apiPath + img.Z5_Image.url}
              alt=""
            />
            <ImageLayers
              className="HeroImageZ6"
              src={apiPath + img.Z6_Image.url}
              alt=""
            />
          </BottlesWrap>
          <ImageLayers src={apiPath + img.Z4_Image.url} alt="" />
          {renderScroll()}
        </ImageWrap>
      )}
      {device === 'mobile' && imgMob.Z1_Image && (
        <ImageWrap>
          <ImageMob
            src={apiPath + imgMob.Z1_Image.url}
            alt="Знакомьтесь, pulpy!"
            onLoad={() => setImgMobLoad(true)}
          />
          <ImageLayersMob src={apiPath + imgMob.Z2_Image.url} alt="" />
          {renderScroll()}
        </ImageWrap>
      )}
    </Section>
  );
};

export default Hero;
