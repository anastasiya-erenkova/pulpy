import styled from 'styled-components';

import shine from './shine.png';
import shineMob from './shine-mob.png';

const bottom = '250px';
const bottomDesk = '200px';
const bottomTablet = '180px';

export const Section = styled.section`
  ${({ theme, isVisible }) => `
    position: relative;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    padding-bottom: ${bottom};
    background-color: ${theme.color.primary};
    
    ::before {
      content: '';
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      background-image: url('${shine}');
      background-repeat: no-repeat;
      background-size: 100% auto;
      background-position: center top;
      transition: 0.9s ease;
      opacity: ${isVisible ? '1' : '0'};
      visibility: ${isVisible ? 'visible' : 'hidden'};
      transform: ${isVisible ? 'translateY(0)' : 'translateY(-20%)'};
      
      ${theme.media.mobile} {
        background-image: url('${shineMob}');
        background-position: center;
        background-size: 110%;    
        opacity: 0;
        visibility: visible;
        transform: translateY(-20%);
        ${isVisible ? 'animation: wrapMobile 0.5s linear 0.7s' : ''};
        animation-fill-mode: forwards;
      }
    }

    ${theme.media.desktop} {
      padding-bottom: ${bottomDesk};
    }

    ${theme.media.tablet} {
      padding-bottom: ${bottomTablet};
    }

    ${theme.media.mobile} {
      display: flex;
      justify-content: center;
      padding-bottom: 100px;
      padding-top: 0;
      margin-top: 0;
      min-height: ${document.documentElement.clientHeight}px;
      box-sizing: content-box;
      align-items: center;
    }

    &.finish-animation {
      ${ImageWrap} > ${ImageLayers} {
        :nth-child(2) {
          opacity: 1;
        }
        :nth-child(4) {
          opacity: 1;
        }
      }

      ${BottlesWrap} {
        img {
          opacity: 1;
        }
      }
    }

    ${ImageWrap} {
      ${theme.media.mobile} {      
        opacity: 0;
        transform: translateY(-20%);
        ${isVisible ? 'animation: wrapMobile 0.5s linear 0.7s' : ''};
        animation-fill-mode: forwards;
      }
    }
    
    ${ImageWrap} > ${ImageLayers} {
      :nth-child(2) {
        opacity: 0;
        ${isVisible ? 'animation: fruits 7s linear 0.1s' : ''};
      }

      :nth-child(4) {
        opacity: 0;
        ${isVisible ? 'animation: text 6s ease 0.1s' : ''};
      }
    }
    
    ${BottlesWrap} {
      img {
        opacity: 0;
        ${isVisible ? 'animation: bottles 7s linear 0.1s' : ''};
        
        :nth-child(2) {
          ${isVisible ? 'animation: bottles 7s linear 0.1s' : ''};
        }
        :nth-child(3) {
          ${isVisible ? 'animation: bottles 7s linear 0.1s' : ''};
        }
      }
    }
  `}

  @keyframes wrapMobile {
    0% {
      opacity: 0;
      transform: translateY(-20%);
    }
    100% {
      opacity: 1;
      transform: translateY(0);
    }
  }

  @keyframes fruits {
    0% {
      opacity: 1;
      transform: translateY(-50%) scale(0);
    }
    10% {
      transform: translateY(0%) rotate(-7deg) scale(0.5);
    }
    20% {
      transform: rotate(5deg) scale(1);
    }
    30% {
      transform: rotate(-3deg) scale(1);
    }
    40% {
      transform: rotate(3deg) scale(1);
    }
    50% {
      transform: rotate(-3deg) scale(1);
    }
    60% {
      transform: rotate(3deg) scale(1);
    }
    70% {
      transform: rotate(-3deg) scale(1);
    }
    80% {
      transform: rotate(-2deg) scale(1);
    }
    100% {
      transform: rotate(-2deg) scale(1);
      opacity: 1;
    }
  }

  @keyframes text {
    0% {
      opacity: 1;
      transform: rotate(-30deg) scale(0);
    }
    20% {
      transform: rotate(15deg) scale(0.5);
    }
    40% {
      transform: rotate(-7deg) scale(1.2);
    }
    60% {
      opacity: 1;
      transform: rotate(0) scale(1);
    }
  }

  @keyframes textMobile {
    0% {
      transform: rotate(-30deg) scale(0);
    }
    20% {
      transform: rotate(15deg) scale(0.5);
    }
    40% {
      transform: rotate(-7deg) scale(1.2);
    }
    60% {
      transform: rotate(0) scale(1);
    }
  }

  @keyframes bottles {
    0% {
      opacity: 1;
      transform: translateY(100%) scale(0.95);
    }
    20% {
      transform: translateY(0%) rotate(-3deg) scale(0.95);
    }
    40% {
      transform: rotate(4deg) scale(0.95);
    }
    60% {
      transform: rotate(-1deg) scale(0.95);
    }
    70% {
      transform: rotate(4deg) scale(0.97);
    }
    80% {
      transform: rotate(2deg) scale(1);
    }
    100% {
      opacity: 1;
      transform: rotate(2deg) scale(1);
    }
  }
`;

const Image = styled.img`
  max-width: 100%;
  max-height: 100%;
`;

export const ImageWrap = styled.div`
  position: relative;
  display: ${({ device }) => (device === 'desktop' ? 'inline-flex' : 'none')};
  justify-content: center;
  align-items: flex-end;
  padding-top: 7.5%;
  margin-top: -7.5%;
  max-width: 58%;

  ${({ theme, device }) => `
    ${theme.media.mobile} {
      min-width: 615px;
      display: ${device !== 'desktop' ? 'inline-flex' : 'none'};
      padding-top: 20px;
      padding-bottom: 20px;
      margin-top: -18%;
    }
    
    > ${ImageLayers} {
      :nth-child(2) {
        transform: rotate(-2deg);
      }
    }
  `};
`;

export const ImageDesk = styled(Image)`
  z-index: 1;
  max-height: 1020px;
`;

export const ImageLayers = styled.img`
  position: absolute;
  left: -17%;
  bottom: 0;
  max-height: calc(100% - 7.5%);
  width: 134%;

  :nth-child(2) {
    z-index: 2;
  }
  :nth-child(3) {
    z-index: 3;
  }
  :nth-child(4) {
    z-index: 4;
  }

  ${({ theme }) => `
    ${theme.media.mobile} {
      display: none
    }
  `}
`;

export const ImageLayersMob = styled.img`
  position: absolute;
  left: -17%;
  bottom: 0;
  max-height: calc(100% - 7.5%);
  width: 134%;

  :nth-child(2) {
    z-index: 2;
  }

  ${({ theme }) => `
    ${theme.media.mobile} {
      max-height: 100%;
      width: 100%;
      left: 0;
    }
  `}
`;

export const BottlesWrap = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  width: 100%;
  border-radius: 0 0 50% 50%;
  overflow: hidden;
  z-index: 4;
  transform: rotate(-2deg);

  ::-webkit-scrollbar {
    display: none;
  }

  img {
    width: 132%;
    max-height: none;
    bottom: auto;
    top: 7.5%;
    left: -15.5%;
    transform: rotate(2deg);

    :nth-child(2) {
      z-index: 10;
    }
  }
`;

export const ImageMob = styled(Image)``;

export const Scroll = styled.a`
  position: absolute;
  bottom: 0;
  left: 50%;
  transform: translate(-50%, 38%);
  display: inline-block;
  z-index: 100;

  ${({ theme }) => `
    ${theme.media.desktop} {
      transform: translate(-50%, 50%);
    }

    ${theme.media.mobile} {
      bottom: 20px;
      width: 55px;
      height: 55px;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    ::before {
      content: '';
      display: inline-block;
      width: 65px;
      height: 65px;
      background-color: ${theme.color.secondary};
      border-radius: 50%;

      ${theme.media.desktop} {
        width: 55px;
        height: 55px;
      }

      ${theme.media.mobile} {
        width: 36px;
        height: 36px;
      }
    }
    
    svg {
      position: absolute;
      top: 50%;
      left: 50%;
      stroke-width: 4;
      animation: arrow 2s linear infinite;
      width: 38px;
      height: 22px;
      
      ${theme.media.desktop} {
        width: 25px;
        height: 15px;
      }
      
      ${theme.media.mobile} {
        width: 19px;
        height: 11px;
      }
    }
  `}

  @keyframes arrow {
    0% {
      transform: translate(-50%, -50%);
    }
    50% {
      transform: translate(-50%, -25%);
    }
    100% {
      transform: translate(-50%, -50%);
    }
  }
`;
