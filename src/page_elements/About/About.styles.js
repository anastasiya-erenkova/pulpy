import styled from 'styled-components';

import CustomContainer from '../../components/Container';
import CustomButton from '../../components/Button';
import CustomDescription from '../../components/Description';

export const Section = styled.section`
  ${({ theme, isVisible }) => `
    position: relative;
    margin: -90px 0 -52px;
    padding-top: 80px;

    ::before {
      content: '';
      position: absolute;
      top: 50px;
      bottom: 0;
      right: 0;
      left: 0;
      background: ${theme.linear.tertiary};
      transform: skew(0, -2deg);
    }

    > * {
      position: relative;
    }
    
    ${Info} , ${ImageWrap} {
      transition: 1.2s ease;
      opacity: ${isVisible ? '1' : '0'};
      visibility: ${isVisible ? 'visible' : 'hidden'};
    }
    
    ${Info} {
      transform: ${isVisible ? 'translateX(0)' : 'translateX(-100%)'};
      
      ${theme.media.tablet} {
        transform: ${isVisible ? 'translateY(0)' : 'translateY(50%)'};
      }
    }
    
    ${ImageWrap} {
      transform: ${isVisible ? 'translateX(0)' : 'translateX(90%)'};
      
      ${theme.media.tablet} {
        transform: ${isVisible ? 'translateY(0)' : 'translateY(50%)'};
      }
    }
  `}
`;

export const Container = styled(CustomContainer)`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  max-width: calc(1440px + 140px * 2);
  padding: 0 140px;

  @media (max-width: 1700px) {
    align-items: center;
  }

  ${({ theme }) => `
    ${theme.media.tablet} {
      flex-direction: column-reverse;
      padding: 0 18px;
    }
  `}
`;

export const Title = styled.h2`
  text-transform: uppercase;
  font-size: 100px;
  line-height: 1.2;

  @media (max-width: 1700px) {
    font-size: 90px;
  }

  @media (max-width: 1600px) {
    font-size: 80px;
  }

  ${({ theme }) => `
    ${theme.media.desktop} {
      font-size: 60px;
    }
  
    ${theme.media.mobile} {
      font-size: 36px;
    }
  `}
`;

export const ImageWrap = styled.div`
  position: relative;
  flex-shrink: 0;
  margin: -11% 0 -10% -16%;

  @media (max-width: 1700px) {
    max-width: 80%;
    margin: -9% 0 -10% -10%;
  }

  @media (max-width: 1700px) {
    margin-left: -12%;
  }

  ${({ theme }) => `
    ${theme.media.tablet} {
      margin-left: -4%;
    }
    
    ${theme.media.mobile} {
      max-width: 600px;
      width: 166%;
      flex-shrink: 0;
      margin: -100px 0 -85px -6%;
    }
  `}
`;

export const Image = styled.img`
  max-width: 100%;
`;

export const ImageLayer = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;

  :nth-child(2) {
    z-index: 2;

    ${({ theme }) => `
      ${theme.media.mobile} {
        top: -23px;
        left: -21px;
      }
    `}
  }

  :nth-child(3) {
    z-index: 3;

    ${({ theme }) => `
      ${theme.media.mobile} {
        left: 33px;
      }
    `}
  }
`;

export const Info = styled.div`
  max-width: 585px;
  flex-shrink: 0;
  padding: 230px 0 360px;
  z-index: 100;

  @media (max-width: 1700px) {
    padding: 180px 0 300px;
  }

  ${({ theme }) => `
    ${theme.media.desktop} {
      max-width: 47%;
      padding: 150px 0 240px;
    }
    
    @media (max-width: 1300px) {
      padding: 150px 0 210px;
      max-width: 57%;
    }
    
    @media (max-width: 1200px) {
      padding: 130px 0 190px;
    }

    ${theme.media.tablet} {
      padding-top: 0;
      text-align: center;
      padding-bottom: 170px;
      max-width: 75%;
    }

    ${theme.media.mobile} {
      width: 95%;
      max-width: 400px;
      padding-bottom: 160px;
    }
  `}
`;

export const Description = styled(CustomDescription)`
  margin: 30px 0;

  @media (max-width: 1600px) {
    font-size: 28px;
    margin: 25px 0;
  }

  ${({ theme }) => `
    ${theme.media.desktop} {
      font-size: 24px;
    }
    
    @media (max-width: 1200px) {
      font-size: 22px;
    }

    ${theme.media.tablet} {
      margin: 15px 0;
    }

    ${theme.media.mobile} {
      font-size: 18px;
      margin: 12px 0;
    }
  `}
`;

export const Button = styled(CustomButton)`
  @media (max-width: 1600px) {
    font-size: 30px;
  }

  ${({ theme }) => `
    ${theme.media.desktop} {
      font-size: 26px;
      padding: 20px 58px;
    }
  
    ${theme.media.mobile} {
      font-size: 22px;
      padding: 13px 36px;
    }
  `}
`;
