import React, { useState, useEffect } from 'react';

import sections from '../../helpers/sections';

import image from './image.png';
import image2 from './image2.png';
import image3 from './image3.png';
import {
  Section,
  Container,
  Info,
  Title,
  Description,
  ImageWrap,
  Image,
  ImageLayer,
  Button,
} from './About.styles';

const About = () => {
  const [isVisible, setVisible] = useState(false);

  useEffect(() => {
    document.addEventListener('scroll', () => {
      const el = document.querySelector(`#${sections.about}`);

      if (
        el &&
        el.getBoundingClientRect().height * 0.6 -
          el.getBoundingClientRect().top >
          0
      ) {
        setVisible(true);
      }
    });
  }, []);

  return (
    <Section id={sections.about} isVisible={isVisible}>
      <Container>
        <Info>
          <Title>Это палпи</Title>
          <Description>
            Сокосодержащий напиток с&nbsp;цельной мякотью — «палпинками» и
            кусочками фруктов, который подарит взрывной вкус и&nbsp;наполнит
            яркими эмоциями
          </Description>
          <Button forwardedAs="a" href={`#${sections.products}`}>
            Подробнее
          </Button>
        </Info>
        <ImageWrap>
          <Image src={image} alt="О бренде" />
          <ImageLayer className="AboutImageZ2" src={image2} alt="" />
          <ImageLayer className="AboutImageZ3" src={image3} alt="" />
        </ImageWrap>
      </Container>
    </Section>
  );
};

export default About;
