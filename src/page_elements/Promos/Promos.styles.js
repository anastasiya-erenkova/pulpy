import styled, { createGlobalStyle } from 'styled-components';

export const Section = styled.section`
  ${({ theme }) => `
    padding: 52px 0 100px;
    background: ${theme.linear.primary};
    position: relative;
    z-index: 100;
    text-align: center;
    margin-top: -2px;

    ${theme.media.mobile} {
      padding: 30px 0 0;
    }
  `}
`;

export const SliderStyles = createGlobalStyle`
  ${({ theme }) => `
    .swiper-container {
      overflow: visible;
      margin-bottom: -142px;
      
      ${theme.media.mobile} {
        margin-bottom: -45px;
      }
      
      .swiper-wrapper {
        display: flex;
        align-items: flex-end;
      }
  
      .swiper-pagination {
        position: absolute;
        bottom: 140px;
        display: inline-block;
        width: auto;
        left: 59.5%;
        
        @media (max-width: 1600px) {
          bottom: 80px;
        }
        
        ${theme.media.tablet} {
          left: 50%;
          transform: translateX(-50%);
          max-width: 100%;
          bottom: 75px;
        }
  
        .swiper-pagination-bullet {
          width: 30px;
          height: 30px;
          display: inline-flex;
          align-items: center;
          justify-content: center;
          cursor: pointer;
          background: none;
          opacity: 1;
          margin-right: 30px;

          :last-child {
            margin-right: 0;
          }
                    
          ${theme.media.desktop} {
            margin-left: 0;
            width: 22px;
            height: 22px;
          }
          
          ${theme.media.tablet} {
            margin-right: 0;
            width: 28px;
            height: 28px;
          }
          
          ::before {
            content: '';
            display: inline-block;
            background-color: ${theme.color.secondary};
            border-radius: 50%;
            width: 20px;
            height: 20px;
            opacity: 0.4;
            transition: opacity .2s ease;
            
            ${theme.media.desktop} {
              width: 14px;
              height: 14px;
            }
            
            ${theme.media.tablet} {
              width: 8px;
              height: 8px;
            }
          }

          :hover {
            ::before {
              opacity: 1;
            }
          }
        }
  
        .swiper-pagination-bullet-active {
          ::before {
            opacity: 1;
          }
        }
      }
    }
  `}
`;

export const Modal = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;
