import React, { useEffect, useState } from 'react';
import 'swiper/css/swiper.css';
import Slider from 'react-id-swiper';

import sections from '../../helpers/sections';

import Promo from './Promo';
import Product from './PromoProduct';

import { SliderStyles, Section } from './Promos.styles';

const Promos = ({ promos, products }) => {
  const [device, setDevice] = useState(null);
  const [swiper, updateSwiper] = useState(null);

  const defineDevice = () => {
    if (window.innerWidth < 1024) {
      setDevice('mobile');
    } else {
      setDevice('desktop');
    }
  };

  useEffect(() => {
    defineDevice();
    window.addEventListener('resize', defineDevice);
    const gif = document.querySelector(`#${sections.gif}`);
    if (gif) {
      gif.addEventListener('click', () => {
        const bullet = document.querySelector('.swiper-pagination-bullet');
        bullet && bullet.click();
      });
    }
  }, []);

  const renderPromo = (promo) => (
    <Promo key={promo.id} promo={promo} isInit={!!swiper} />
  );

  const renderProduct = (product) => (
    <Product key={product.id} product={product} device={device} />
  );

  const filtredPromo = promos.filter((promo) => promo.IsVisible);
  const filtredProducts = products.filter((product) => product.IsVisible);

  const isOneSlide = filtredPromo.length + filtredProducts.length < 2;

  const sliderSettings = {
    loop: isOneSlide ? false : true,
    noSwiping: isOneSlide ? true : false,
    autoplay: {
      delay: 10000,
      disableOnInteraction: true,
    },
    pagination: isOneSlide
      ? {}
      : {
          el: '.swiper-pagination',
          clickable: true,
        },
    rebuildOnUpdate: true,
  };

  if (!promos.length && !products.length) return null;

  return (
    <Section id={sections.promo}>
      <SliderStyles />
      <Slider {...sliderSettings} getSwiper={updateSwiper}>
        {filtredPromo.map(renderPromo)}
        {filtredProducts.map(renderProduct)}
      </Slider>
    </Section>
  );
};
export default Promos;
