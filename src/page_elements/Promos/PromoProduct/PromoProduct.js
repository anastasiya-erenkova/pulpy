import React from 'react';

import apiPath from '../../../helpers/api';
import normalizeTitle from '../../../helpers/normalizeText';

import {
  Item,
  Content,
  Back,
  BackMobile,
  Info,
  Title,
  Description,
  Buttons,
  Button,
  Image,
  ImageMobile,
} from './PromoProduct.styles';

const PromoProduct = ({ product, device, ...props }) => {
  if (!product) return null;

  const {
    Background,
    MobileBackground,
    PromotionImage,
    MobileProductLogo,
    Description: descr,
    product: matchProduct,
    PlayLink,
    HeaderText,
    HeaderTextForecolor,
    productInfoLink,
  } = product;

  return (
    <>
      <Item {...props}>
        <Content>
          {device === 'desktop' && (
            <>
              <Back src={apiPath + Background.url} alt="" />
              <Image src={apiPath + PromotionImage.url} alt="" />
            </>
          )}
          {device === 'mobile' && (
            <>
              <BackMobile src={apiPath + MobileBackground.url} alt="" />
              <ImageMobile src={apiPath + MobileProductLogo.url} alt="" />
            </>
          )}
          <Info>
            <Title color={HeaderTextForecolor}>
              {normalizeTitle(HeaderText)}
            </Title>
            <Description>{descr}</Description>
            <Buttons>
              {PlayLink && (
                <Button forwardedAs="a" target="_blank" href={PlayLink}>
                  Играть
                </Button>
              )}
              {(productInfoLink || matchProduct) && (
                <Button
                  importance="outline"
                  forwardedAs="a"
                  target="_blank"
                  href={
                    productInfoLink
                      ? productInfoLink
                      : `#product-${matchProduct.id}`
                  }
                >
                  О продукте
                </Button>
              )}
            </Buttons>
          </Info>
        </Content>
      </Item>
    </>
  );
};

export default PromoProduct;
