import styled from 'styled-components';

import CustomDescription from '../../../components/Description';
import CustomButton from '../../../components/Button';

export const Item = styled.div`
  position: relative;
`;

export const Content = styled.div`
  ${({ theme }) => `
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    top: -40px;
    max-width: ${theme.size.xxl};
    margin:  auto;
    
    ${theme.media.tablet} {
      flex-direction: column;
      overflow: hidden;
    }
  `}
`;

const BackCommon = styled.img`
  ${({ theme }) => `
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    max-width: 100%;
  `}
`;

export const Back = styled(BackCommon)`
  ${({ theme }) => `
    ${theme.media.tablet} {
      display: none;
    }
  `}
`;

export const BackMobile = styled(BackCommon)`
  ${({ theme }) => `
    display: none;
  
    ${theme.media.tablet} {
      display: block;
      top: 0;
      bottom: auto;
      left: auto;
      min-width: 100%;
      min-height: 100%;
    }
  `}
`;

export const Image = styled.img`
  ${({ theme }) => `
    z-index: 100;
    width: 57%;
    align-self: flex-end;
    margin-left: 50px;
    
    ${theme.media.desktop} {
      max-width: 55%;
      margin-left: 30px;
    }
    
    ${theme.media.tablet} {
      display: none;
    }
  `}
`;

export const ImageMobile = styled.img`
  ${({ theme }) => `
    display: none;
    z-index: 100;
    
    ${theme.media.tablet} {
      display: block;
      width: 95%;
      min-width: 460px;
    }
    
    ${theme.media.mobile} {
      position: relative;
      top: 20px;
      left: -10px;
    }
  `}
`;

export const Buttons = styled.div`
  ${({ theme }) => `
    display:flex;
    align-items:center;
    
    > * {
      margin-right: 10px;
      
      :last-child{
        margin-right: 0;
      }
    }
    
    ${theme.media.tablet} {
      justify-content: center;
    }
    
    ${theme.media.mobile} {
      > * {
        margin-right: 15px;
        
        :last-child{
          margin-right: 0;
        }
      }
    }
  `}
`;

export const Button = styled(CustomButton)`
  ${({ theme }) => `
    min-width: 255px;

    ${theme.media.desktop} {
      min-width: 180px;
      font-size: 26px;

      @media (min-width: 1024px) {
        padding: 18px 35px;
      }
    }
    
    ${theme.media.tablet} {
      min-width: 240px;
    }
    
    ${theme.media.mobile} {
      min-width: 160px;
      padding: 15px 20px;
      font-size: 22px;
    }
  `}
`;

export const Info = styled.div`
  ${({ theme }) => `
    position: relative;
    width: 41%;
    padding: 215px 135px 120px 15px;
    text-align: left;
    display: flex;
    flex-direction: column;
    justify-content: center;
    
    @media (max-width: 1600px) {
      padding-right: 100px;
    }
    
    ${theme.media.desktop} {
      padding: 125px 70px 65px 0;
    }
    
    ${theme.media.tablet} {
      width: 100%;
      text-align: center;
      padding: 30px 18px 90px;
    }
    
    ${theme.media.mobile} {
      padding: 20px 18px 70px;
    }
  `}
`;

export const Title = styled.h4`
  ${({ theme, color }) => `
    font-size: 80px;
    line-height: 1.2;
    color: ${color ? color : theme.color.secondary};
    
    @media (max-width: 1600px) {
      font-size: 70px;
    }
    
    ${theme.media.tablet} {
      font-size: 40px;
      text-align: center;
    }
    
    ${theme.media.mobile} {
      font-size: 36px;
    }
  `}
`;

export const Description = styled(CustomDescription)`
  ${({ theme }) => `
    color: #414042;
    margin: 40px 0 45px;
    
    @media (max-width: 1600px) {
      margin-bottom: 35px;
    }
    
    ${theme.media.desktop} {
      margin: 15px 0 20px;
    }
    
    ${theme.media.tablet} {
      margin: 20px 0 25px;
    }
    
    ${theme.media.mobile} {
      margin: 15px 0 20px;
    }
  `}
`;
