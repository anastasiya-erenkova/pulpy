import styled from 'styled-components';

import CustomDescription from '../../../components/Description';
import CustomButton from '../../../components/Button';

import play from './play.png';

const skewY = '2deg';
const skewYTablet = '1deg';

export const Item = styled.div`
  ${({ theme }) => `
    position: relative;
    overflow: hidden;
    transform: skew(0, -${skewY});
    
    ${theme.media.tablet} {
      transform: skew(0, -${skewYTablet});
    }
  `}
`;

export const Button = styled(CustomButton)`
  ${({ theme }) => `
    min-width: 280px;
    
    ${theme.media.desktop} {
      min-width: auto;
      font-size: 32px;
      
      @media (min-width: 1024px) {
        padding: 18px 45px;
      }
    }
    
    ${theme.media.mobile} {
      font-size: 22px;
    }
  `}
`;

export const Content = styled.div`
  ${({ theme }) => `
    display: flex;
    justify-content: center;
    overflow: hidden;
    transform: skew(0, ${skewY});
    position: relative;
    top: -40px;
    max-width: ${theme.size.xxl};
    margin:  auto;
    
    ${theme.media.tablet} {
      flex-direction: column;
      transform: skew(0, ${skewYTablet});
    }
  `}
`;

export const Video = styled.div`
  ${({ theme }) => `
    position: relative;
    top: 30px;
    width: 55%;
    text-align: right;
    cursor: pointer;
    
    ${theme.media.tablet} {
      width: 100%;
    }
  `}
`;

export const VideoPlay = styled.button.attrs({ type: 'button' })`
  ${({ theme }) => `
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: white;
    border-radius: 50%;
    width: 131px;
    height: 131px;
    transition: 0.2s ease;
    
    ${theme.media.desktop} {
      width: 115px;
      height: 115px;
    }
    
    ${theme.media.mobile} {
      width: 56px;
      height: 56px;
    }
    
    :hover {
      background-color: #eee;
    }

    ::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      background-image: url(${play});
      background-repeat: no-repeat;
      background-size: contain;
      width: 34px;
      height: 39px;
      transform: translate(-35%, -50%);
      
      ${theme.media.desktop} {
        width: 30px;
        height: 35px;
      }
      
      ${theme.media.mobile} {
        width: 17px;
        height: 20px;
      }
    }
  `}
`;

export const VideoImage = styled.img`
  max-width: 100%;
`;

export const Info = styled.div`
  ${({ theme }) => `
    position: relative;
    width: 45%;
    text-align: left;
    display: flex;
    flex-direction: column;
    justify-content: center;
    
    ${theme.media.tablet} {
      width: 100%;
    }
  `}
`;

export const InfoBack = styled.img`
  ${({ theme }) => `
    position: absolute;
    left: -50px;
    right: 0;
    top: 0;
    bottom: 0;
    transform: skew(6deg,0);
    min-width: 120%;
    min-height: 100%;
    
    ${theme.media.tablet} {
      transform: skew(0,1deg);
    }
  `}
`;

export const InfoContent = styled.div`
  ${({ theme }) => `
    position: relative;
    padding: 140px 87px 110px;
    max-width: 700px;
    
    ${theme.media.desktop} {
      padding: 95px 80px 80px 30px;
    }
    
    ${theme.media.tablet} {
      max-width: 100%;
      padding: 50px 26px 100px;
      text-align: center;
    }
  `}
`;

export const Title = styled.h4`
  ${({ theme }) => `
    font-size: 80px;
    line-height: 1.2;
    color: white;
    
    @media (max-width: 1600px) {
      font-size: 70px;
    }
    
    ${theme.media.tablet} {
      font-size: 40px;
      text-align: center;
    }
    
    ${theme.media.mobile} {
      font-size: 36px;
    }
  `}
`;

export const Description = styled(CustomDescription)`
  ${({ theme }) => `
    margin: 40px 0 30px;
    
    ${theme.media.desktop} {
      margin: 25px 0 20px;
    }
    
    ${theme.media.tablet} {
      margin: 15px 0 20px;
    }
  `}
`;
