import React, { useState, useEffect } from 'react';

import apiPath from '../../../helpers/api';
import normalizeTitle from '../../../helpers/normalizeText';
import ModalVideo from '../../ModalVideo';

import {
  Item,
  Content,
  Video,
  VideoImage,
  Info,
  InfoContent,
  InfoBack,
  Title,
  Description,
  VideoPlay,
  Button,
} from './Promo.styles';
import sections from '../../../helpers/sections';

const Promo = ({ promo, isInit, ...props }) => {
  const [isOpenVideo, setOpenVideo] = useState(false);

  const openVideo = () => setOpenVideo(true);
  const closeVideo = () => setOpenVideo(false);

  useEffect(() => {
    if (isInit) {
      document
        .querySelector(`#${sections.promo}`)
        .addEventListener('click', (e) => {
          const videos = document.querySelectorAll(
            `.swiper-slide .open-promo-${promo.id}`,
          );
          videos.forEach((video) => {
            if (e.target.parentElement === video) {
              openVideo();
            }
          });
        });
    }
  }, [isInit, promo.id]);

  if (!promo) return null;

  return (
    <>
      <Item {...props}>
        <Content>
          <Video className={`open-promo-${promo.id}`}>
            <VideoImage
              src={apiPath + promo.YoutubeVideoBackground.url}
              alt=""
            />
            <VideoPlay />
          </Video>
          <Info>
            <InfoBack src={apiPath + promo.Background.url} alt="" />
            <InfoContent>
              <Title>{normalizeTitle(promo.HeaderText)}</Title>
              <Description>{promo.Description}</Description>
              {promo.WatchMoreLink && (
                <Button
                  forwardedAs="a"
                  href={promo.WatchMoreLink}
                  target="_blank"
                >
                  Смотреть
                </Button>
              )}
            </InfoContent>
          </Info>
        </Content>
      </Item>
      {isOpenVideo ? (
        <ModalVideo src={promo.YoutubeVideo} onClose={closeVideo} />
      ) : null}
    </>
  );
};

export default Promo;
