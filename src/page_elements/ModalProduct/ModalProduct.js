import React from 'react';

import apiPath from '../../helpers/api';
import Inner from './Inner';

import { Modal, Container, Close, Back } from './ModalProduct.styles';

const ModalProduct = ({ product, onClose }) => {
  const { InfoBackground, id } = product;

  return (
    <Modal id={`product-modal-${id}`}>
      <Back src={apiPath + InfoBackground.url} alt="" />
      <Container>
        <Close onClick={onClose} />
        <Inner product={product} />
      </Container>
    </Modal>
  );
};

export default ModalProduct;
