import styled from 'styled-components';

import Button from '../Button';

export const Content = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-grow: 1;
  width: 100%;
  flex-shrink: 0;

  ${({ theme, headerColor, textColor }) => `
    ${theme.media.tablet} {
      flex-direction: column-reverse;
    }
    
    ${Title} {
      color: ${headerColor ? headerColor : 'inherit'};
    }
    
    ${Description} {
      color: ${textColor ? textColor : 'inherit'};
    }
    
  `}
`;

export const ImageWrap = styled.div`
  position: relative;
  width: 50%;
  flex-shrink: 0;
  display: flex;
  align-items: center;
  left: 8%;
  top: -75px;
  margin-bottom: -175px;

  @media (max-width: 1600px) {
    width: 40%;
    left: 4%;
    top: -60px;
  }

  ${({ theme }) => `
    ${theme.media.tablet} {
      left: 8%;
      top: -75px;
      width: 100%;
      margin-bottom: -120px;
      margin-top: -60px;
    }
  
    ${theme.media.mobile} {
      margin: -155px 0 -40px;
      position: relative;
      left: 42px;
      top: auto;
      max-width: 400px;
    }
  `}
`;

export const Image = styled.img`
  transform: translateX(-50%);
  left: 50%;
  position: relative;
  max-width: 150%;

  ${({ theme }) => `
    ${theme.media.tablet} {
      max-width: 115%;
    }
    
    ${theme.media.mobile} {
      max-width: 213%;
    }
  `}
`;

export const Label = styled.img`
  position: absolute;
  left: 40%;
  bottom: 19%;
  transform: translateX(-50%);
  width: 298px;

  @media (max-width: 1600px) {
    width: 210px;
    left: 42%;
  }

  ${({ theme }) => `
    ${theme.media.tablet} {
      width: 215px;
      left: 43%;
    }
    
    ${theme.media.mobile} {
      width: 205px;
      left: 40%;
    }
  `}
`;

export const Info = styled.div`
  width: 50%;
  flex-shrink: 0;
  padding: 35px 10px 10px;
  z-index: 100;

  > * {
    max-width: 755px;
  }

  @media (max-width: 1600px) {
    padding-right: 80px;
    width: 60%;
  }

  ${({ theme }) => `
    ${theme.media.tablet} {
      width: 100%;
      max-width: 800px;
      padding: 50px 70px;
      text-align: center;
      
      > * {
        max-width: none;
      }
    }
    
    ${theme.media.mobile} {
      width: 90%;
      padding: 50px 0;
    }
  `}
`;

export const Title = styled.h3`
  font-size: 58px;
  line-height: 1.2;
  margin-bottom: 20px;

  @media (max-width: 1600px) {
    font-size: 35px;
    margin-bottom: 15px;
  }

  ${({ theme }) => `
    ${theme.media.mobile} {
      font-size: 24px;
      margin-bottom: 15px;
    }
  `}
`;

export const Description = styled.p`
  font-family: GothamPro;
  font-size: 24px;
  line-height: 1.3;
  margin-bottom: 40px;

  @media (max-width: 1600px) {
    font-size: 20px;
  }

  ${({ theme }) => `
    ${theme.media.mobile} {
      font-size: 16px;
      line-height: 1.4;
    }
  `}
`;

const SubTitle = styled.p`
  font-size: 36px;
  line-height: 1.2;

  @media (max-width: 1600px) {
    font-size: 30px;
  }

  ${({ theme }) => `
    ${theme.media.mobile} {
      font-size: 24px;
    }
  `}
`;

export const Ingridients = styled.div`
  display: flex;

  ${({ theme, ingrColor }) => `
    color: ${ingrColor ? ingrColor : 'inherit'};
  
    ${theme.media.tablet} {
      flex-direction: column;
    }
  `}
`;

export const IngridientsBlock = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(50% - 5px);
  margin-right: 10px;

  :last-child {
    margin-right: 0;
  }

  ${({ theme }) => `    
    ${theme.media.desktop} {
      width: 100%;
      margin-right: 0;
    }
  `}
`;

export const Ingridient = styled.p`
  position: relative;
  margin-bottom: 45px;
  display: flex;
  align-items: center;

  span {
    font-family: GothamPro;
    font-size: 24px;
    line-height: 1.5;
    padding-left: 42px;
  }

  img {
    position: absolute;
    left: 21px;
    top: 50%;
    transform: translate(-50%, -50%);
    max-height: 35px;
    max-width: 35px;
    flex-shrink: 0;
  }

  ${({ theme }) => `    
    ${theme.media.desktop} {
      margin-bottom: 30px;

      span {
        font-size: 20px;
      }
    }
    
    ${theme.media.tablet} {      
      justify-content: center;

      span {
        padding-left: 0;
      }
      
      img {
        position: static;
        transform: translate(0, 0);
        margin-right: 8px;
      }
    }
  
    ${theme.media.mobile} {
      margin-bottom: 12px;
      
      span {
        font-size: 17px;
      }
      
      img {
        max-height: 23px;
        max-width: 23px;
      }
    }
  `}
`;

export const IngridientsDesktop = styled.div`
  ${({ theme }) => `
    ${theme.media.tablet} {
      display: none;
    }
  `}
`;

export const IngridientsMobile = styled.div`
  display: none;
  position: relative;

  ${({ theme }) => `
    ${theme.media.tablet} {
      display: block;
      text-align: center;
    }
  `}
`;

export const IngridientsTitle = styled(SubTitle)`
  margin-bottom: 30px;

  ${({ theme, headerColor }) => `    
    color: ${headerColor ? headerColor : 'inherit'};

    ${theme.media.desktop} {
      margin-bottom: 25px;
    }
    
    ${theme.media.mobile} {
      margin-bottom: 20px;
    }
  `}
`;

export const Shops = styled.div`
  position: relative;
  text-align: center;

  ${({ theme }) => `
    ${theme.media.tablet} {
      margin-top: 25px;
    }
    
    ${theme.media.mobile} {
      margin-top: 15px;
    }
  `}
`;

export const ShopList = styled.div`
  display: flex;
  justify-content: center;

  ${({ theme }) => `
    ${theme.media.tablet} {
      flex-direction: column;
      align-items: center;
    }
  `}
`;

export const Shop = styled(Button)`
  padding-left: 5px;
  padding-right: 5px;
  margin-right: 15px;
  width: 288px;
  height: 86px;

  :last-child {
    margin-right: 0;
  }

  span {
    display: flex;
  }

  img {
    position: relative;
    max-width: 100%;
    max-height: 100%;
  }

  ${({ theme }) => `
    @media (max-width: 1600px) {
      width: 260px;
      height: 77px;
    }

    ${theme.media.tablet} {
      margin-right: 0;
      margin-bottom: 20px;
    }
  
    ${theme.media.mobile} {
      margin-bottom: 10px;
      min-width: auto;
      width: 260px;
      height: 48px;
    }
  `}
`;

export const ShopsTitle = styled(SubTitle)`
  margin-bottom: 25px;

  ${({ theme, headerColor }) => `
    color: ${headerColor ? headerColor : 'inherit'};
    
    ${theme.media.desktop} {
      margin-bottom: 20px;
    }
  `}
`;
