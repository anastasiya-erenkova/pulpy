import React, { useEffect, useState } from 'react';
import apiPath from '../../../helpers/api';
import normalizeName from '../../../helpers/normalizeText';

import label from './new.png';
import {
  Ingridient,
  IngridientsBlock,
  Content,
  Description,
  Image,
  ImageWrap,
  Info,
  Ingridients,
  IngridientsDesktop,
  IngridientsMobile,
  IngridientsTitle,
  Label,
  ShopList,
  Shops,
  Shop,
  ShopsTitle,
  Title,
} from './Inner.styles';

const renderIngridient = (ingridient) => (
  <Ingridient key={ingridient.id}>
    <img src={apiPath + ingridient.Icon.url} alt="" />
    <span>{ingridient.DisplayName}</span>
  </Ingridient>
);

const renderIngridients = (ingridients) => {
  if (ingridients.length > 3) {
    const middleCount = Math.ceil(ingridients.length / 2);

    return (
      <>
        <IngridientsBlock>
          {ingridients
            .filter((item, index) => index < middleCount)
            .map(renderIngridient)}
        </IngridientsBlock>
        <IngridientsBlock>
          {ingridients
            .filter((item, index) => index >= middleCount)
            .map(renderIngridient)}
        </IngridientsBlock>
      </>
    );
  }

  return (
    <IngridientsBlock>{ingridients.map(renderIngridient)}</IngridientsBlock>
  );
};

const renderShop = (shop) => (
  <Shop forwardedAs="a" href={shop.Link} key={shop.Name} target="_blank">
    <img src={apiPath + shop.Logo.url} alt={shop.Name} />
  </Shop>
);

const Inner = ({ product }) => {
  const [device, setDevice] = useState(null);

  const defineDevice = () => {
    if (window.innerWidth < 768) {
      setDevice('mobile');
    } else {
      setDevice('desktop');
    }
  };

  useEffect(() => {
    defineDevice();
    window.addEventListener('resize', defineDevice);
  }, []);

  const {
    HeaderText,
    DescriptionText,
    HeaderForeColor,
    ForeColor,
    InfoLogo,
    ingridients,
    IsNewProduct,
  } = product;

  return (
    <>
      <Content headerColor={HeaderForeColor} textColor={ForeColor}>
        <ImageWrap>
          <Image src={apiPath + InfoLogo.url} alt="" />
          {IsNewProduct && <Label src={label} alt="new" />}
        </ImageWrap>
        <Info>
          <Title>{normalizeName(HeaderText)}</Title>
          <Description>{DescriptionText}</Description>
          {!!ingridients.length && device === 'desktop' && (
            <IngridientsDesktop>
              <IngridientsTitle headerColor={HeaderForeColor}>
                Ингредиенты
              </IngridientsTitle>
              <Ingridients ingrColor={ForeColor}>
                {renderIngridients(ingridients)}
              </Ingridients>
            </IngridientsDesktop>
          )}
        </Info>
      </Content>
      {!!ingridients.length && device === 'mobile' && (
        <IngridientsMobile>
          <IngridientsTitle headerColor={HeaderForeColor}>
            Ингредиенты
          </IngridientsTitle>
          <Ingridients ingrColor={ForeColor}>
            {renderIngridients(ingridients)}
          </Ingridients>
        </IngridientsMobile>
      )}
      {!!product.Shop.length && (
        <Shops>
          <ShopsTitle headerColor={HeaderForeColor}>Где купить</ShopsTitle>
          <ShopList>{product.Shop.map(renderShop)}</ShopList>
        </Shops>
      )}
    </>
  );
};

export default Inner;
