import React from 'react';

const Back = ({ importance, ...props }) =>
  importance === 'outline' ? (
    <svg
      {...props}
      viewBox="0 0 276 85"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M11.8788 83.9834L1.15997 15.841L274.844 1.06386L264.099 79.1284L11.8788 83.9834Z" />
    </svg>
  ) : (
    <svg
      {...props}
      viewBox="0 0 276 85"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M0 15L275.5 0L265.5 80L10 85L0 15Z" />
    </svg>
  );

export default Back;
