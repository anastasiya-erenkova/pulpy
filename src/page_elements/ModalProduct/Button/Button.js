import React from 'react';

import { CustomButton, Back, BackMobile } from './Button.styles';

const Button = ({ children, ...props }) => (
  <CustomButton {...props}>
    <Back importance={props.importance} />
    <BackMobile />
    {children}
  </CustomButton>
);

export default Button;
