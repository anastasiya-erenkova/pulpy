import React from 'react';

const BackMobile = ({ importance, ...props }) => (
  <svg
    {...props}
    viewBox="0 0 260 48"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M0 8.41535L260 0L249.613 45.2396L10.3872 48L0 8.41535Z" />
  </svg>
);

export default BackMobile;
