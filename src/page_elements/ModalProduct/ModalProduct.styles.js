import styled from 'styled-components';

import CustomModal from '../../components/Modal';
import CustomClose from './Close';

export const Modal = styled(CustomModal)`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 6000;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0;
  visibility: hidden;
  transition: opacity 0.3s linear, visibility 0.3s linear;

  &.open {
    opacity: 1;
    visibility: visible;
  }
`;

export const Container = styled.div`
  max-height: 100vh;
  width: 100%;
  max-width: 100%;
  overflow-y: auto;
  overflow-x: hidden;
  padding: 5px 20px 20px;
  display: flex;
  flex-direction: column;
  flex-shrink: 0;

  ${({ theme }) => `
    ${theme.media.mobile} {
      padding: 20px 25px;
    }
  `}
`;

export const Close = styled(CustomClose)`
  position: absolute;
  top: 44px;
  right: 43px;
  z-index: 2000;

  ${({ theme }) => `
    ${theme.media.tablet} {
      top: 17px;
      right: 10px;
    }
  `}
`;

export const Back = styled.img`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  min-height: 100%;
  min-width: 100%;
`;
