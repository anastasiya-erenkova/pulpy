import styled from 'styled-components';

export const CustomClose = styled.button.attrs({ type: 'button' })`
  ${({ theme }) => `
    position: relative;
    padding: 0;
  
    ${theme.media.tablet} {
      padding: 10px;
    }

    ::before {
      content: '';
      display: inline-block;
      width: 55px;
      height: 55px;
      border-radius: 50%;
      background-color: white;
      transition: 0.2s ease;

      ${theme.media.tablet} {
        width: 40px;
        height: 40px;
      }
    }
    
    svg {
      position: absolute;
      flex-shrink: 0;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      transition: 0.2s ease;
      fill: ${theme.color.secondary};
      width: 25px;
      height: 25px;
      
      ${theme.media.tablet} {
        width: 19px;
        height: 19px;
      }
    }
    
    :hover {
      ::before {
        background-color: ${theme.color.secondary};
      }
      
      svg {
        fill: white;
      }
    }
  `}
`;
