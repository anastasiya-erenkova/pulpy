import React from 'react';

import { CustomClose } from './Close.styles';

const Close = ({ ...props }) => (
  <CustomClose {...props}>
    <svg viewBox="0 0 39 38" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M37.1801 3.90186L5.52701 37.642L2.36214 32.4588L32.5187 1.25874L37.1801 3.90186Z" />
      <path d="M4.65229 1.04351L38.3924 32.6966L33.2092 35.8615L2.00917 5.70495L4.65229 1.04351Z" />
    </svg>
  </CustomClose>
);

export default Close;
