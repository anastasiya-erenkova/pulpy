import styled from 'styled-components';

import CustomModal from '../../components/Modal';

export const Modal = styled(CustomModal)`
  ${({ theme }) => `
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 5000;
    background-color: rgba(0, 0, 0, 0.7);
    padding: 20px;

    ${theme.media.mobile} {
      padding: 20px 10px;
    }
  `}
`;

export const Container = styled.div`
  //width: 100%;
  //height: 100%;
  //display: flex;
  //align-items: center;
  //justify-content: center;
  text-align: center;
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  padding: 0 8px;

  ::before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
  }
`;

export const Content = styled.div`
  line-height: 0;
  width: 100%;
  max-width: 900px;
  position: relative;
  display: inline-block;
  vertical-align: middle;
  margin: 0 auto;
  text-align: left;
  z-index: 1045;
`;

export const Scaler = styled.div`
  width: 100%;
  height: 0;
  //overflow: hidden;
  padding-top: 56.25%;
`;

export const Close = styled.button.attrs({ type: 'button' })`
  position: absolute;
  padding: 5px 10px;
  top: -45px;
  right: -10px;

  ::before {
    content: '×';
    font-size: 28px;
    opacity: 0.65;
    color: #fff;
    transition: opacity 0.15s ease;
  }

  :hover::before {
    opacity: 1;
  }
`;

export const Iframe = styled.iframe`
  position: absolute;
  display: block;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  box-shadow: 0 0 8px rgba(0, 0, 0, 0.6);
  background: #000;
`;
