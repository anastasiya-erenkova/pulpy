import React from 'react';

import {
  Modal,
  Iframe,
  Container,
  Content,
  Scaler,
  Close,
} from './ModalVideo.styles';

const ModalVideo = ({ src, onClose }) => (
  <Modal>
    <Container onClick={(e) => e.currentTarget === e.target && onClose()}>
      <Content>
        <Scaler>
          <Close onClick={(e) => e.currentTarget === e.target && onClose()} />
          <Iframe
            src={src}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </Scaler>
      </Content>
    </Container>
  </Modal>
);

export default ModalVideo;
