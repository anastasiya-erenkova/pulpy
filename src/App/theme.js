const theme = {
  color: {
    primary: '#76C0BD',
    secondary: '#EA5304',
  },
  linear: {
    primary:
      'linear-gradient(270deg, #28B4B4 0%, #5AC3BE 30%, #78E1DC  96.82%)',
    tertiary:
      'linear-gradient(240.55deg, #7BB4DF 5.26%, #62ABDC 37.53%, #62ABDC 37.54%, #458ECA 76.48%)',
    sidebar:
      'linear-gradient(310.74deg, #69B4E3 -3.68%, #36A8E0 43.18%, #2190CF 84.05%)',
  },
  media: {
    mobile: '@media (max-width: 767px)',
    tablet: '@media (max-width: 1023px)',
    desktop: '@media (max-width: 1439px)',
  },
  size: {
    xxl: '1920px',
    xl: '1440px',
    md: '740px',
  },
  transition: {
    primary: '0.5s ease',
  },
};

export default theme;
