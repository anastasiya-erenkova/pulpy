import React from 'react';
import { ThemeProvider } from 'styled-components';

import Router from '../router';

import { GlobalStyles } from './App.styles';
import theme from './theme';

const App = () => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyles />
      <Router />
    </>
  </ThemeProvider>
);

export default App;
