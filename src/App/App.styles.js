import { createGlobalStyle } from 'styled-components';
import { reset } from './reset';

import './fonts/Firenight/style.css';
import './fonts/GothamPro/style.css';

export const GlobalStyles = createGlobalStyle`
  ${reset}
  
  * {
    box-sizing: border-box;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }

  html {
    overflow-x: hidden;

    &.fixed {
      overflow: hidden; 
    }
  }

  body {
    margin: 0;
    font-family: "Firenight", sans-serif;
    color: white;
    overflow-x: hidden;
    overflow-y: hidden;
  }

  button:focus,
  a:focus {
    outline: none;
  }

  button {
    cursor: pointer;
    background: none;
    border: none;
    font-family: inherit;
    color: inherit;
  }

  a {
    text-decoration: none;
    color: inherit;
    cursor: pointer;
  }
`;
