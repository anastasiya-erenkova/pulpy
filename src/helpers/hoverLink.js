import { css } from 'styled-components';

const hoverLink = css`
  position: relative;

  ::after {
    content: '';
    position: absolute;
    left: 10px;
    bottom: 7px;
    width: calc(100% - 20px);
    height: 2px;
    background-color: white;
    transform: scale(0);
    transition: transform 0.25s ease;
  }

  :hover::after {
    transform: scale(1);
  }
`;

export default hoverLink;
