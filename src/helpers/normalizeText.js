import React from 'react';

const normalizeText = (name) => {
  const text = name.split('\\n');
  const normalize = (item, index) => <span key={index}>{item}</span>;

  return text.map(normalize);
};

export default normalizeText;
