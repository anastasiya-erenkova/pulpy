const addScroll = () => {
  const links = document.querySelectorAll('[href^="#"]');

  const linkListner = (link) => {
    link.addEventListener('click', (e) => {
      e.preventDefault();
      const id = e.currentTarget.getAttribute('href').replace('#', '');
      const element = document.getElementById(id);
      element && element.scrollIntoView({ behavior: 'smooth' });
    });
  };

  for (let i = 0; i < links.length; i++) {
    linkListner(links[i]);
  }
};

export default addScroll;
