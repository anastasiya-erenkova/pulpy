const sections = {
  hero: 'hero',
  about: 'about',
  products: 'products',
  promo: 'promo',
  gif: 'gif',
};

export default sections;
