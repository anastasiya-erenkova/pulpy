import React from 'react';
import { BrowserRouter as RouterReact, Switch, Route } from 'react-router-dom';

import Main from '../pages/Main';
import NotFound from '../pages/NotFound';

const Router = () => (
  <RouterReact>
    <Switch>
      <Route exact path="/" component={Main} />
      <Route path="/404" component={NotFound} />
    </Switch>
  </RouterReact>
);

export default Router;
